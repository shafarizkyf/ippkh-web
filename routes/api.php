<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'Auth\LoginController@loginJwt')->name('api.login');

Route::prefix('workflow')->group(function(){
  Route::post('update/{id}', 'ProgressWorkflowController@update')->name('progress_workflow.update');
});

Route::group(['middleware'=>['jwt.auth']], function(){
  Route::get('shortcut', 'ShortcutController@list')->name('api.shortcut.shortcut');
  Route::prefix('pengajuan')->group(function(){
    Route::post('pemanfaatan/create', 'PengajuanPemanfaatanController@create')->name('api.pemanfaatan.create');

    Route::prefix('penggunaan')->group(function(){
      Route::get('', 'PengajuanPenggunaanController@index')->name('api.penggunaan.index');
      Route::get('detail/{id}', 'PengajuanPenggunaanController@detail')->name('api.penggunaan.detail');
      Route::post('create', 'PengajuanPenggunaanController@store')->name('api.penggunaan.store');
      Route::get('files/{id}', 'DokumenController@show')->name('api.files.index');
      Route::get('surat-surat/{id}', 'SuratController@index')->name('api.surat-surat.index');

      Route::prefix('peninjauan-lapangan')->group(function(){
        Route::post('surat-perintah/{id}', 'SuratController@storeSuratPerintahPeninjauanLapangan')->name('api.penggunaan.peninjauan.surat-perintah.store');
        Route::post('tagihan/{id}', 'TagihanPeninjauanLapanganController@store')->name('api.penggunaan.peninjauan.tagihan.store');
        Route::post('undangan/{id}', 'PeninjauanLapanganController@store')->name('api.penggunaan.peninjauan.undangan.store');
      });
    });
  });

  Route::get('bidang-kegiatan', 'BidangKegiatanController@list')->name('api.bidang-kegiatan.index');
  Route::get('jenis-izin', 'JenisIzinController@list')->name('api.jenis-izin.index');
  Route::get('jenis-kegiatan', 'JenisKegiatanController@list')->name('api.jenis-kegiatan.index');
  Route::get('jenis-pemohon', 'JenisPemohonController@list')->name('api.jenis-pemohon.index');
  Route::get('mekanisme', 'MekanismeController@list')->name('api.mekanisme.index');
  Route::get('unit-kerja', 'UnitKerjaController@index')->name('api.unit-kerja.index');
  Route::get('kph', 'KphController@index')->name('api.kph.index');
  Route::get('provinsi', 'ProvinsiController@index')->name('api.provinsi.index');
});
