<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', 'Auth\LoginController@login')->name('login');
Route::post('login', 'Auth\LoginController@login')->name('login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('reset', 'Auth\LoginController@reset')->name('reset');
Route::get('/', 'Auth\LoginController@home')->name('home');
Route::get('android/policy', 'Auth\LoginController@policy');

Route::middleware(['auth'])->group(function(){
  Route::get('/', 'Auth\LoginController@home')->name('home');
  Route::get('dashboard', 'DashboardController@index')->name('dashboard');

  Route::prefix('pengajuan')->group(function(){
    Route::prefix('penggunaan')->group(function(){
      Route::get('', 'PengajuanPenggunaanController@index')->name('penggunaan.index');
      Route::get('baru', 'PengajuanPenggunaanController@create')->name('penggunaan.create');
      Route::post('baru', 'PengajuanPenggunaanController@store')->name('penggunaan.store');

      Route::get('edit/{id}', 'PengajuanPenggunaanController@edit')->name('penggunaan.edit');
      Route::post('update/{id}', 'PengajuanPenggunaanController@update')->name('penggunaan.update');
      Route::post('delete/{id}', 'PengajuanPenggunaanController@delete')->name('penggunaan.delete');

      Route::post('simpanProgressPengajuan', 'PengajuanPenggunaanController@simpanProgressPengajuan')->name('penggunaan.simpanProgressPengajuan');
      Route::post('editProgressPengajuan', 'PengajuanPenggunaanController@editProgressPengajuan')->name('penggunaan.editProgressPengajuan');
      Route::get('detail/{id}', 'PengajuanPenggunaanController@detail')->name('penggunaan.detail');
      Route::get('getProgressForm/{id}', 'PengajuanPenggunaanController@getProgressForm')->name('penggunaan.get.form.progress');
      Route::get('getProgressFormEdit/{id}', 'PengajuanPenggunaanController@getProgressFormEdit')->name('penggunaan.get.form.progress.edit');
      Route::get('deleteProgress/{id}', 'PengajuanPenggunaanController@deleteProgress')->name('penggunaan.delete.progress');

      Route::prefix('peninjauan-lapangan')->group(function(){
        Route::post('surat-perintah/{id}', 'SuratController@storeSuratPerintahPeninjauanLapangan')->name('penggunaan.peninjauan.surat-perintah.store');
        Route::post('tagihan/{id}', 'TagihanPeninjauanLapanganController@store')->name('penggunaan.peninjauan.tagihan.store');
        Route::post('undangan/{id}', 'PeninjauanLapanganController@store')->name('penggunaan.peninjauan.undangan.store');
      });
    });

    Route::prefix('pemanfaatan')->group(function(){
      Route::get('', 'PengajuanPemanfaatanController@index')->name('pemanfaatan.index');
      Route::get('baru', 'PengajuanPemanfaatanController@create')->name('pemanfaatan.create');
      Route::post('baru', 'PengajuanPemanfaatanController@store')->name('pemanfaatan.store');
    });
  });

  Route::prefix('workflow')->group(function(){
    Route::get('', 'ProgressWorkflowController@index')->name('progress_workflow.index');
    Route::post('', 'ProgressWorkflowController@store')->name('progress_workflow.store');
    Route::get('add', 'ProgressWorkflowController@add')->name('progress_workflow.add');
    Route::get('{id}', 'ProgressWorkflowController@edit')->name('progress_workflow.edit');
    Route::post('{id}', 'ProgressWorkflowController@delete')->name('progress_workflow.delete');
  });

  Route::prefix('progress')->group(function(){
    Route::get('', 'ProgressController@index')->name('progress.index');
    Route::get('add', 'ProgressController@add')->name('progress.add');
    Route::get('{id}', 'ProgressController@edit')->name('progress.edit');
    Route::post('', 'ProgressController@store')->name('progress.store');
    Route::post('update/{id}', 'ProgressController@update')->name('progress.update');
    Route::post('{id}', 'ProgressController@delete')->name('progress.delete');
  });

  Route::prefix('pengguna')->group(function(){
    Route::get('', 'UserController@index')->name('pengguna.index');
    Route::post('', 'UserController@store')->name('pengguna.store');
    Route::post('delete/{id}', 'UserController@delete')->name('pengguna.delete');
    Route::post('update/{id}', 'UserController@update')->name('pengguna.update');
  });

  Route::prefix('role')->group(function(){
    Route::get('', 'RoleController@index')->name('role.index');
    Route::get('baru', 'RoleController@create')->name('role.create');
    Route::get('edit/{id}', 'RoleController@edit')->name('role.edit');
    Route::post('', 'RoleController@store')->name('role.store');
    Route::post('delete/{id}', 'RoleController@delete')->name('role.delete');
    Route::post('update/{id}', 'RoleController@update')->name('role.update');
  });

  Route::prefix('bidang-kegiatan')->group(function(){
    Route::get('', 'BidangKegiatanController@index')->name('bidang-kegiatan.index');
    Route::post('', 'BidangKegiatanController@store')->name('bidang-kegiatan.store');
    Route::post('delete/{id}', 'BidangKegiatanController@delete')->name('bidang-kegiatan.delete');
    Route::post('update/{id}', 'BidangKegiatanController@update')->name('bidang-kegiatan.update');
  });

  Route::prefix('jenis-izin')->group(function(){
    Route::get('', 'JenisIzinController@index')->name('jenis-izin.index');
    Route::post('', 'JenisIzinController@store')->name('jenis-izin.store');
    Route::post('delete/{id}', 'JenisIzinController@delete')->name('jenis-izin.delete');
    Route::post('update/{id}', 'JenisIzinController@update')->name('jenis-izin.update');
  });

  Route::prefix('jenis-kegiatan')->group(function(){
    Route::get('', 'JenisKegiatanController@index')->name('jenis-kegiatan.index');
    Route::post('', 'JenisKegiatanController@store')->name('jenis-kegiatan.store');
    Route::post('delete/{id}', 'JenisKegiatanController@delete')->name('jenis-kegiatan.delete');
    Route::post('update/{id}', 'JenisKegiatanController@update')->name('jenis-kegiatan.update');
  });

  Route::prefix('jenis-pemohon')->group(function(){
    Route::get('', 'JenisPemohonController@index')->name('jenis-pemohon.index');
    Route::post('', 'JenisPemohonController@store')->name('jenis-pemohon.store');
    Route::post('delete/{id}', 'JenisPemohonController@delete')->name('jenis-pemohon.delete');
    Route::post('update/{id}', 'JenisPemohonController@update')->name('jenis-pemohon.update');
  });

  Route::prefix('mekanisme')->group(function(){
    Route::get('', 'MekanismeController@index')->name('mekanisme.index');
    Route::post('', 'MekanismeController@store')->name('mekanisme.store');
    Route::post('delete/{id}', 'MekanismeController@delete')->name('mekanisme.delete');
    Route::post('update/{id}', 'MekanismeController@update')->name('mekanisme.update');
  });

  Route::prefix('reminder')->group(function(){
    Route::get('', 'ReminderController@index')->name('reminder.index');
    Route::post('', 'ReminderController@store')->name('reminder.store');
    Route::post('delete/{id}', 'ReminderController@delete')->name('reminder.delete');
    Route::post('update/{id}', 'ReminderController@update')->name('reminder.update');
  });

  Route::prefix('shortcut')->group(function(){
    Route::get('', 'ShortcutController@index')->name('shortcut.index');
    Route::get('add', 'ShortcutController@add')->name('shortcut.add');
    Route::get('edit/{id}', 'ShortcutController@edit')->name('shortcut.edit');
    Route::post('', 'ShortcutController@store')->name('shortcut.store');
    Route::post('delete/{id}', 'ShortcutController@delete')->name('shortcut.delete');
    Route::post('update/{id}', 'ShortcutController@update')->name('shortcut.update');
  });

  Route::get('file/{id}', 'FileController@stream')->name('file.stream');

  Route::get('unit-kerja', 'UnitKerjaController@index')->name('unit-kerja.index');
  Route::get('kph', 'KphController@index')->name('kph.index');
  Route::get('provinsi', 'ProvinsiController@index')->name('provinsi.index');

});

