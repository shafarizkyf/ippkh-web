@extends('layouts.admin')

@section('page-title')
List Jabatan
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card-box table-responsive">
      <button class="btn btn-primary btn-sm waves-effect waves-light" id="btn-add" data-route="{{ route('jabatan.store') }}" data-toggle="modal" data-target=".bs-example-modal-lg">
        <span class="ti-plus"></span> Tambah
      </button>
      <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>Jabatan</th>
            <th>Total</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @foreach($jabatan as $o)
          <tr>
            <td>{{ $o->nama }}</td>
            <td>{{ $o->users->count() }} Pengguna</td>
            <td>
              {{-- <button class="btn btn-info btn-sm waves-effect waves-light" data-toggle="modal" data-target=".modal-detail"><span class="ti-eye"></span> Detail</button> --}}
              <button class="btn btn-warning btn-sm waves-effect waves-light btn-edit" data-route="{{ route('jabatan.update', $o->id) }}" data-jabatan="{{ $o->nama }}" data-toggle="modal" data-target=".bs-example-modal-lg">
                <span class="ti-pencil"></span> Edit
              </button>
              <form action="{{ route('jabatan.delete', $o->id) }}" class="form-delete" method="POST" style="display: inline">
                {{ csrf_field() }}
                <button class="btn btn-danger btn-delete btn-sm waves-effect waves-light">
                  <span class="ti-trash"></span>  Hapus
                </button>
              </form>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection

@section('modal')
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myLargeModalLabel">Tambah Jabatan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <form action="{{ route('jabatan.store') }}" id="form-add" method="POST" class="form-horizontal" role="form">
          {{ csrf_field() }}
          <div class="form-group">
            <label for="jabatan">Jabatan</label>
            <input type="text" class="form-control" name="nama" id="jabatan" placeholder="Jabatan" required>
          </div>
          <div class="form-group">
            <button class="btn btn-primary btn-sm pull-right"><span class="ti-check"></span> Simpan</button>
          </div>
        </form>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade modal-detail" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myLargeModalLabel">List Pengguna</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">

      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
@endsection

@section('css-top')
  <!-- Sweet Alert css -->
  <link href="{{ asset('template/assets/plugins/sweet-alert/sweetalert2.min.css') }}"  rel="stylesheet" type="text/css" />
  <!-- DataTables -->
  <link href="{{ asset('template/assets/plugins/datatables/dataTables.bootstrap4.min.css') }}"  rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/plugins/datatables/buttons.bootstrap4.min.css') }}"  rel="stylesheet" type="text/css" />
  <!-- Responsive datatable examples -->
  <link href="{{ asset('template/assets/plugins/datatables/responsive.bootstrap4.min.css') }}"  rel="stylesheet" type="text/css" />
  <!-- Select2 css -->
  <link href="{{ asset('template/assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('js-bottom')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.repeater/1.2.1/jquery.repeater.min.js"></script>
  <!-- Sweet Alert Js  -->
  <script src="{{ asset('template/assets/plugins/sweet-alert/sweetalert2.min.js') }}" type="text/javascript"></script>
  <!-- Required datatable js -->
  <script src="{{ asset('template/assets/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.bootstrap4.min.js') }}" type="text/javascript"></script>
  <!-- Buttons examples -->
  <script src="{{ asset('template/assets/plugins/datatables/dataTables.buttons.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('template/assets/plugins/datatables/buttons.bootstrap4.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('template/assets/plugins/datatables/jszip.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('template/assets/plugins/datatables/pdfmake.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('template/assets/plugins/datatables/vfs_fonts.js') }}" type="text/javascript"></script>
  <script src="{{ asset('template/assets/plugins/datatables/buttons.html5.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('template/assets/plugins/datatables/buttons.print.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('template/assets/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script>
  <script>
    $(function(){

      var form = $('#form-add');
      form.find('.select2').select2();

      var table = $('#datatable-buttons').DataTable({
        lengthChange: false,
        buttons: ['copy', 'excel', 'pdf']
      }).buttons().container().appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');

      $('#btn-add').detach().appendTo('.dt-buttons');

      $('.btn-edit').on('click', function(event){
        var route = $(this).data('route');
        var jabatan = $(this).data('jabatan');

        $('#myLargeModalLabel').html('Edit Jabatan');
        $('#form-add input[name="nama"]').val(jabatan);
        $('#form-add').attr('action', route);
      });

      $('#btn-add').on('click', function(event){
        var route = $(this).data('route');

        $('#myLargeModalLabel').html('Tambah Jabatan');
        $('#form-add input[name="nama"]').val(null);
        $('#form-add').attr('action', route);
      });

      $('.btn-delete').on('click', function(event){
        event.preventDefault();
        var form = $(this).parent();

        swal({
          title: 'Anda akan menghapus data ini?',
          text: "Anda tidak dapat mengembalikan data yang telah dihapus!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#4fa7f3',
          cancelButtonColor: '#d57171',
          confirmButtonText: 'Ya'
        }).then(function (confirm) {
          if(confirm){
            form.submit();
          }
        }).catch(function(dismiss){
          //when cancel or overlay
        });
      });

    });
  </script>
@endsection