@extends('layouts.admin')

@section('page-title')
  List Pengajuan Pemanfaatan
@endsection

@section('content')
<div class="row">
  <div class="col-xl-12">
    <div class="card-box">
      <h4 class="header-title m-t-0 m-b-30">Form Pengajuan Pemanfaatan Hutan</h4>
      <form id="myWizardForm" method="POST" action="{{ route('pemanfaatan.store') }}" class="form-horizontal repeater" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div id="rootwizard" class="pull-in">
          <ul class="nav nav-tabs nav-justified">
            <li class="nav-item"><a href="#first" data-toggle="tab" class="nav-link">Pemohon</a></li>
            <li class="nav-item"><a href="#second" data-toggle="tab" class="nav-link">Pengajuan</a></li>
            <li class="nav-item"><a href="#third" data-toggle="tab" class="nav-link">Wilayah</a></li>
          </ul>
          <!-- DATA PEMOHON -->
          <div class="tab-content mb-0 b-0">
            <div class="tab-pane fade" id="first">
              <div class="row">
                <div class="col-md-6">
                  <div class="control-group m-b-10">
                    <label class="control-label" for="nama">Nama Perusahaan/Instansi</label>
                    <div class="controls">
                      <input type="text" id="nama" name="nama" class="required form-control">
                    </div>
                  </div>
                  <div class="control-group m-b-10">
                    <label class="control-label" for="npwp">NPWP</label>
                    <div class="controls">
                      <input type="text" id="npwp" name="npwp" class="required form-control">
                    </div>
                  </div>
                  <div class="control-group m-b-10">
                    <label class="control-label" for="jenis_pemohon">Jenis Pemohon</label>
                    <div class="controls">
                      <select name="id_jenis_pemohon" id="jenis_pemohon" class="required form-control select2">
                        <option value="">Pilih Jenis Pemohon</option>
                        @foreach($jenisPemohon as $o)
                          <option value="{{ $o->id }}">{{ $o->nama }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>                    
                </div>
                <div class="col-md-6">
                  <div class="control-group m-b-10">
                    <label class="control-label" for="telp">Nomor telp kantor</label>
                    <div class="controls">
                      <input type="text" id="telp" name="telp" class="required form-control">
                    </div>
                  </div>                
                  <div class="control-group m-b-10">
                    <label class="control-label" for="nama_wali">Nama Wali</label>
                    <div class="controls">
                      <input type="text" id="nama_wali" name="nama_wali" class="required form-control">
                    </div>
                  </div>                
                  <div class="control-group m-b-10">
                    <label class="control-label" for="telp_wali">Nomor Telepon Wali</label>
                    <div class="controls">
                      <input type="text" id="telp_wali" name="telp_wali" class="required form-control">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- DATA PENGAJUAN -->
            <div class="tab-pane fade" id="second">
              <div class="row">
                <div class="col-md-6">
                  <div class="control-group m-b-10">
                    <label class="control-label" for="kegiatan">Nama Kegiatan</label>
                    <div class="controls">
                      <input type="text" id="kegiatan" name="kegiatan"class="required form-control">
                    </div>
                  </div>
                  <div class="control-group m-b-10">
                    <label class="control-label" for="nomor">Nomor Surat</label>
                    <div class="controls">
                      <input type="text" id="nomor" name="nomor"class="required form-control">
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="control-group m-b-10">
                    <label class="control-label" for="surat">File Surat</label>
                    <div class="controls">
                      <input type="file" name="surat" class="required form-control" accept="application/pdf">
                    </div>
                  </div>
                  <div class="control-group m-b-10">
                    <label class="control-label" for="proposal">File Proposal</label>
                    <div class="controls">
                      <input type="file" name="proposal" class="required form-control" accept="application/pdf">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- DATA WILAYAH -->
            <div class="tab-pane fade" id="third" style="margin-bottom:100px;">
              <div data-repeater-list="wilayah">
                <div data-repeater-item>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="control-group m-b-10">
                        <label class="control-label" for="provinsi">Provinsi</label>
                        <div class="controls">
                          <select name="wilayah[][provinsi]" id="provinsi" class="required form-control select2">
                            <option value="">Pilih Provinsi</option>
                            @foreach($provinsi as $o)
                              <option value="{{ $o->id }}">{{ $o->nama }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="control-group m-b-10">
                        <label class="control-label" for="unit_kerja">Unit Kerja</label>
                        <div class="controls">
                          <select name="wilayah[][unit_kerja]" id="unit_kerja" class="required form-control select2" data-route="{{ route('kph.index') }}">
                            <option value="">Pilih Unit Kerja</option>
                            @foreach($unitKerja as $o)
                              <option value="{{ $o->id }}">{{ $o->nama }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="control-group m-b-10">
                        <label class="control-label" for="kph">KPH</label>
                        <div class="controls">
                          <select name="wilayah[][kph]" id="kph" class="required form-control select2">
                            <option value="">Pilih KPH</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>                            
                  <hr>
                  <button data-repeater-delete class="btn btn-sm btn-rounded btn-danger pull-right"><span class="ti-minus"></span></button>
                </div>
              </div>
              <button data-repeater-create class="btn btn-sm btn-rounded btn-success pull-right m-r-10"><span class="ti-plus"></span></button>
            </div>
            <ul class="list-inline wizard mb-0 mt-4">
              <li class="previous list-inline-item"><a href="#" class="btn btn-primary waves-effect waves-light">Sebelumnya</a></li>
              <li class="next list-inline-item float-right"><a href="#" class="btn btn-primary waves-effect waves-light">Selanjutnya</a></li>
            </ul>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('css-top')
  <link href="{{ asset('template/assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('js-bottom')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.repeater/1.2.1/jquery.repeater.min.js"></script>
  <script src="{{ asset('template/assets/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('template/assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/jquery-validation/dist/jquery.validate.min.js') }}"></script>
  <script>
    $(function(){
      var form = $('#myWizardForm');
      form.find('.select2').select2();

      $('.repeater').repeater({
        show: function () {
          $(this).show(function(){
            form.find('select2').next('.select2-container').remove();
            form.find('select2').select2();
            });
          },
          hide: function (remove) {
            $(this).hide(remove);
          }
      });

      $('#unit_kerja').on('change', function(){
        var id_unit_kerja = $(this).val();
        var route = $(this).data('route');
        var kphEl = $('#kph').find('option').remove().end().append('<option>Pilih KPH</option>');

        $.getJSON(route, {id_unit_kerja}).done(function(data){
          $.each(data, function(index, value){
            var newOption = new Option(value.nama, value.id, false, false);
            kphEl.append(newOption).trigger('change');
          });//end each
        });//end getJSON      
      });

      var $validator = form.validate({
        rules: {
          nama: {
            minlength: 3
          },
          npwp: {
            digits:true,
          },
          telp: {
            digits: true,
          },
          nama_wali: {
            minlength: 3
          },
          telp_wali: {
            digits: true
          },
        }
      });

      $('#rootwizard').bootstrapWizard({
        'tabClass': 'nav nav-tabs navtab-wizard nav-justified bg-muted',
        'onNext': function (tab, navigation, index) {

          var $valid = $("#myWizardForm").valid();
          if (!$valid) {
            $validator.focusInvalid();
            return false;
          }

          if(index == 3 && $valid){
            $('#myWizardForm').submit();
          }
        }
      });
    })
  </script>
@endsection