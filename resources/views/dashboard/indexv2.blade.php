@extends('layouts.admin')

@section('page-title')
  Dashboard
  @if(in_array('shortcut.add', Auth::user()->routePermissions()))
    <span style="font-size: 12px; text-decoration: underline">
      <a href="{{ route('shortcut.add') }}"> atur shortcut</a>
    </span>
  @endif
@endsection

@section('content')
  <div class="row">
    @foreach($mekanismes as $mekanisme)
    <div class="col-xl-4 m-b-5">
      @component('components/dashboard/statistic_card', [
          'subtitle' => 'Permohonan',
          'color' => request('mekanisme_id') == $mekanisme->id ? 'bg-secondary' : 'bg-primary',
          'route' => request('mekanisme_id') == $mekanisme->id ? route('dashboard')  : route('dashboard', ['mekanisme_id' => $mekanisme->id])
        ])

        @slot('title')
          {{ $mekanisme->nama }}
        @endslot

        @slot('amount')
          {{ $mekanisme->pengajuanPenggunaan->count() }}
        @endslot
      @endcomponent
    </div>
    @endforeach
  </div>
  <hr />
  <div class="row">
    @foreach($shortcuts as $key => $shortcut)
    @php
      $colors = ['bg-info', 'bg-warning', 'bg-danger', 'bg-purple', 'bg-pink'];
      $key    = $key >= count($colors) ? rand(0, count($colors) - 1) : $key;
      $total  = App\StatisticByMekanisme::whereMekanismeId(request('mekanisme_id'))->whereProgressId($shortcut->progress_id)->first();
    @endphp
    <div class="col-xl-3 m-b-5">
      @component('components/dashboard/statistic_card', [
          'subtitle' => 'Permohonan',
          'route' => route('penggunaan.index', [
              'id_progress' => $shortcut->progress_id,
              'id_mekanisme' => request('mekanisme_id')
            ])
        ])

        @slot('title')
          {{ $shortcut->progress->nama }}
        @endslot

        @slot('color')
          {{ $colors[$key] }}
        @endslot

        @slot('amount')
          {{ $total ? $total->total : 0 }}
        @endslot
      @endcomponent
    </div>
    @endforeach
  </div>
  {{-- @if (count($statistics))
  <hr />
  @endif --}}
  <div class="row m-t-5">
    @foreach($mekanismes as $key => $mekanisme)
      @php
        $colors = ['bg-pink', 'bg-purple', 'bg-danger', 'bg-warning', 'bg-info',];
        $key    = $key >= count($colors) ? rand(0, count($colors) - 1) : $key;
      @endphp
    <div class="col-xl-4 m-b-5">
      @component('components/dashboard/download_card', ['title' => $mekanisme->nama])
        @slot('color')
          {{ $colors[$key] }}
        @endslot
        @if($mekanisme->panduan)
          @slot('route')
            http://{{ $mekanisme->panduan }}
          @endslot
        @endif
      @endcomponent
    </div>
    @endforeach
  </div>
  <div class="row m-t-5">
    <div class="col-md-12">
      <div class="card-box">
        <h4 class="header-title mt-0 mb-3">Pengingat</h4 >
        <div class="table-responsive">
          <table class="table table-hover" id="reminder">
            <thead>
              <tr>
                <th>#</th>
                <th>Pengingat</th>
                <th>Progress</th>
                <th>Pengajuan</th>
                <th>Jatuh Tempo</th>
              </tr>
            </thead>
            <tbody>
              @foreach($reminders as $key => $reminder)
                @if(!App\MyClass\Reusable::compare($reminder->diff_days, $reminder->reminder->operator, $reminder->reminder->hari))
                  @continue;
                @endif
              <tr>
                <td>{{ $key + 1 }}</td>
                <td>{{ $reminder->reminder->nama }}</td>
                <td>{{ $reminder->progress->nama }}</td>
                <td><a href="{{ route('penggunaan.detail', $reminder->pengajuanPenggunaan->id) }}">{{ $reminder->pengajuanPenggunaan->kegiatan }}</a></td>
                <td>{{ $reminder->diff_days }} Hari</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('js-bottom')
<script>
  $(function(){
    var table = $('#reminder').DataTable({
      lengthChange: false,
      buttons: ['copy', 'excel', 'pdf']
    }).buttons().container().appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
  });
</script>
@endsection