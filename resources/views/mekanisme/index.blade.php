@extends('layouts.admin')

@section('page-title')
List Mekanisme
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card-box table-responsive">
      @if(in_array('mekanisme.store', Auth::user()->routePermissions()))
      <button class="btn btn-primary btn-sm waves-effect waves-light" id="btn-add" data-toggle="modal" data-target=".modalAdd">
        <span class="ti-plus"></span> Tambah
      </button>
      @endif
      <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>#</th>
            <th>Nama</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @foreach($mekanismes as $index => $mekanisme)
          <tr>
            <td>{{ $index + 1 }}</td>
            <td>{{ $mekanisme->nama }}</td>
            <td>
              @if($mekanisme->panduan)
              <a class="btn btn-secondary btn-sm" href="http://{{ $mekanisme->panduan }}" target="_blank"> Panduan</a>
              @endif
              @if(in_array('mekanisme.update', Auth::user()->routePermissions()))
              <button class="btn btn-warning btn-edit btn-sm waves-effect waves-light"
                data-toggle="modal"
                data-target=".modalEdit"
                data-nama="{{ $mekanisme->nama }}"
                data-route="{{ route('mekanisme.update', $mekanisme->id) }}">
                <span class="ti-pencil"></span> Ubah
              </button>
              @endif
              @if(in_array('mekanisme.delete', Auth::user()->routePermissions()))
              <form action="{{ route('mekanisme.delete', $mekanisme->id) }}" class="form-delete" method="POST" style="display: inline">
                {{ csrf_field() }}
                <button class="btn btn-danger btn-delete btn-sm waves-effect waves-light">
                  <span class="ti-trash"></span>  Hapus
                </button>
              </form>
              @endif
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection

@section('modal')
<div class="modal fade modalAdd" tabindex="-1" role="dialog" aria-labelledby="modalAdd" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="modalAdd">Tambah Mekanisme</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <form action="{{ route('mekanisme.store') }}" id="form-add" method="POST" class="form-horizontal" enctype="multipart/form-data" autocomplete="off" role="form">
          {{ csrf_field() }}
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="nama">Mekanisme</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="" minlength="3" maxlength="50" required>
              </div>
              <div class="form-group">
                <label for="file">Dokumen Alur Proses Perizinan</label>
                <input type="file" class="form-control" name="file" id="file" accept="images/*,application/pdf" required>
              </div>
              <div class="form-group">
                <button class="btn btn-primary btn-sm pull-right"><span class="ti-check"></span> Simpan</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade modalEdit" tabindex="-1" role="dialog" aria-labelledby="modalEdit" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="modalEdit">Ubah Mekanisme</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <form action="" id="form-edit" method="POST" class="form-horizontal" enctype="multipart/form-data" autocomplete="off" role="form">
          {{ csrf_field() }}
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="nama">Mekanisme</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="" minlength="3" maxlength="50" required>
              </div>
              <div class="form-group">
                <label for="file">Dokumen Alur Proses Perizinan</label>
                <input type="file" class="form-control" name="file" id="file" accept="images/*|application/pdf">
              </div>
              <div class="form-group">
                <button class="btn btn-primary btn-sm pull-right"><span class="ti-check"></span> Simpan</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js-bottom')
  <script>
    $(function(){

      var formAdd = $('#form-add');
      var formEdit = $('#form-edit');

      var table = $('#datatable-buttons').DataTable({
        lengthChange: false,
        buttons: ['copy', 'excel', 'pdf']
      }).buttons().container().appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');

      $('#btn-add').detach().appendTo('.dt-buttons');

      $('tbody').on('click', '.btn-delete', function(event){
        event.preventDefault();
        var form = $(this).parent();

        swal({
          title: 'Anda akan menghapus data ini?',
          text: "Anda tidak dapat mengembalikan data yang telah dihapus!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#4fa7f3',
          cancelButtonColor: '#d57171',
          confirmButtonText: 'Ya'
        }).then(function (confirm) {
          if(confirm){
            form.submit();
          }
        }).catch(function(dismiss){
          //when cancel or overlay
        });
      });

      $('tbody').on('click', '.btn-edit', function(){
        const nama = $(this).data('nama');
        formEdit.find('input[name=nama]').val(nama);
        formEdit.attr('action', $(this).data('route'))
      });

    });
  </script>
@endsection