@extends('layouts.admin')

@section('page-title')
List Pengguna
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card-box table-responsive">
      @if(in_array('pengguna.store', Auth::user()->routePermissions()))
      <button class="btn btn-primary btn-sm waves-effect waves-light" id="btn-add" data-toggle="modal" data-target=".modalAdd">
        <span class="ti-plus"></span> Tambah
      </button>
      @endif
      <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>Nama</th>
            <th>Username</th>
            <th>Jabatan</th>
            @if(in_array('pengguna.update', Auth::user()->routePermissions()) || in_array('pengguna.delete', Auth::user()->routePermissions()))
            <th></th>
            @endif
          </tr>
        </thead>
        <tbody>
          @foreach($users as $user)
          <tr>
            <td>{{ $user->nama }}</td>
            <td>{{ $user->username }}</td>
            <td>{{ $user->role->nama }}</td>
            <td>
              @if(in_array('pengguna.update', Auth::user()->routePermissions()))
              <button class="btn btn-warning btn-edit btn-sm waves-effect waves-light" 
                data-toggle="modal" 
                data-target=".modalEdit"
                data-username="{{ $user->username }}"
                data-nama="{{ $user->nama }}"
                data-role="{{ $user->role_id }}"
                data-unit-kerja="{{ $user->unit_kerja_id }}"
                data-kph="{{ $user->kph_id }}"
                data-route="{{ route('pengguna.update', $user->id) }}">
                <span class="ti-pencil"></span> Ubah
              </button>
              @endif
              @if(in_array('pengguna.delete', Auth::user()->routePermissions()))
              <form action="{{ route('pengguna.delete', $user->id) }}" class="form-delete" method="POST" style="display: inline">
                {{ csrf_field() }}
                <button class="btn btn-danger btn-delete btn-sm waves-effect waves-light">
                  <span class="ti-trash"></span>  Hapus
                </button>
              </form>
              @endif
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection

@section('modal')
<div class="modal fade modalAdd" role="dialog" aria-labelledby="modalAdd" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="modalAdd">Tambah Pengguna</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <form action="{{ route('pengguna.store') }}" id="form-add" method="POST" class="form-horizontal" role="form" autocomplete="off">
          {{ csrf_field() }}
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="username">Username</label>
                <input type="text" class="form-control" name="username" id="username" placeholder="Username" required>
              </div>
              <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" required>
              </div>
              <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" name="password" id="password" placeholder="Password" required>
              </div>
              <div class="form-group">
                <label for="repassword">Konfirmasi Password</label>
                <input type="password" class="form-control" name="repassword" id="repassword" placeholder="Konfirmasi Password" required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="id_jabatan">Pilih Role</label>
                <select name="id_role" id="id_role" class="form-control select2">
                  <option value="">Pilih Role</option>
                  @foreach($role as $o)
                  <option value="{{ $o->id }}">{{ $o->nama }}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label class="col-form-label">Unit Kerja</label>
                <select name="id_unit_kerja" id="unit_kerja" class="form-control select2" data-route="{{ route('kph.index') }}">
                  <option value="">Pilih Unit Kerja</option>
                  @foreach ($unitKerja as $o)
                    <option value="{{ $o->id }}">{{ $o->nama }}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label class="col-form-label">KPH</label>
                <select name="id_kph" id="kph" class="form-control select2">
                  <option value="">Pilih KPH</option>
                </select>
              </div>
              <div class="form-group">
                <button class="btn btn-primary btn-sm pull-right"><span class="ti-check"></span> Simpan</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade modalEdit" role="dialog" aria-labelledby="modalEdit" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="modalEdit">Edit Pengguna</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <form action="" id="form-edit" method="POST" class="form-horizontal" role="form" autocomplete="off">
          {{ csrf_field() }}
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="username">Username</label>
                <input type="text" class="form-control" name="username" placeholder="Username" required>
              </div>
              <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama" placeholder="Nama" required>
              </div>
              <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" name="password" placeholder="Password">
              </div>
              <div class="form-group">
                <label for="repassword">Konfirmasi Password</label>
                <input type="password" class="form-control" name="repassword" placeholder="Konfirmasi Password">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="id_jabatan">Pilih Role</label>
                <select name="id_role" class="form-control select2">
                  <option value="">Pilih Role</option>
                  @foreach($role as $o)
                  <option value="{{ $o->id }}">{{ $o->nama }}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label class="col-form-label">Unit Kerja</label>
                <select name="id_unit_kerja" class="form-control select2" data-route="{{ route('kph.index') }}">
                  <option value="">Pilih Unit Kerja</option>
                  @foreach ($unitKerja as $o)
                    <option value="{{ $o->id }}">{{ $o->nama }}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label class="col-form-label">KPH</label>
                <select name="id_kph" class="form-control select2">
                  <option value="">Pilih KPH</option>
                </select>
              </div>
              <div class="form-group">
                <button class="btn btn-primary btn-sm pull-right"><span class="ti-check"></span> Simpan</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
@endsection

@section('js-bottom')
  <script>
    $(function(){

      var formAdd = $('#form-add');
      var formEdit = $('#form-edit');

      formAdd.find('.select2').select2();
      formEdit.find('.select2').select2();

      $('select[name=id_unit_kerja]').on('change', function(){
        var id_unit_kerja = $(this).val();
        var route = $(this).data('route');
        var kphEl = $('select[name=id_kph]').find('option').remove().end().append('<option value="">Pilih KPH</option>');

        $.getJSON(route, {id_unit_kerja}).done(function(data){
          $.each(data, function(index, value){
            var newOption = new Option(value.nama, value.id, false, false);
            kphEl.append(newOption).trigger('change');
          });//end each
        });//end getJSON
      });

      var table = $('#datatable-buttons').DataTable({
        lengthChange: false,
        buttons: ['copy', 'excel', 'pdf']
      }).buttons().container().appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');

      $('#btn-add').detach().appendTo('.dt-buttons');

      $('tbody').on('click', '.btn-delete', function(event){
        event.preventDefault();
        var form = $(this).parent();

        swal({
          title: 'Anda akan menghapus data ini?',
          text: "Anda tidak dapat mengembalikan data yang telah dihapus!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#4fa7f3',
          cancelButtonColor: '#d57171',
          confirmButtonText: 'Ya'
        }).then(function (confirm) {
          if(confirm){
            form.submit();
          }
        }).catch(function(dismiss){
          //when cancel or overlay
        });
      });

      $('tbody').on('click', '.btn-edit', async function(){
        formEdit.find('input[name=username]').val($(this).data('username'));
        formEdit.find('input[name=nama]').val($(this).data('nama'));

        formEdit.find('select[name=id_role]').val($(this).data('role')).trigger('change')
        formEdit.find('select[name=id_unit_kerja]').val($(this).data('unit-kerja')).trigger('change');
        formEdit.find('select[name=id_kph]').val($(this).data('kph')).trigger('change')

        formEdit.attr('action', $(this).data('route'))
      });

    });
  </script>
@endsection