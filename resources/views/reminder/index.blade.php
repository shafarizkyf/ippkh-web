@extends('layouts.admin')

@section('page-title')
List Reminder
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card-box table-responsive">
      @if(in_array('reminder.store', Auth::user()->routePermissions()))
      <button class="btn btn-primary btn-sm waves-effect waves-light" id="btn-add" data-toggle="modal" data-target=".modalAdd">
        <span class="ti-plus"></span> Tambah
      </button>
      @endif
      <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>#</th>
            <th>Nama</th>
            <th>Progress</th>
            <th>Waktu</th>
            @if(in_array('reminder.update', Auth::user()->routePermissions()) || in_array('reminder.delete', Auth::user()->routePermissions()))
            <th></th>
            @endif
          </tr>
        </thead>
        <tbody>
          @foreach($reminders as $key => $reminder)
          <tr>
            <td>{{ $key + 1 }}</td>
            <td>{{ $reminder->nama }}</td>
            <td>{{ $reminder->progressDetail->progress->nama }}</td>
            <td>{{ $reminder->hari }} Hari</td>
            <td>
              @if(in_array('reminder.update', Auth::user()->routePermissions()))
              <button class="btn btn-warning btn-edit btn-sm waves-effect waves-light"
                data-toggle="modal"
                data-target=".modalEdit"
                data-nama="{{ $reminder->nama }}"
                data-operator="{{ $reminder->operator }}"
                data-progress-detail="{{ $reminder->progress_detail_id }}"
                data-hari="{{ $reminder->hari }}"
                data-route="{{ route('reminder.update', $reminder->id) }}">
                <span class="ti-pencil"></span> Ubah
              </button>
              @endif
              @if(in_array('reminder.delete', Auth::user()->routePermissions()))
              <form action="{{ route('reminder.delete', $reminder->id) }}" class="form-delete" method="POST" style="display: inline">
                {{ csrf_field() }}
                <button class="btn btn-danger btn-delete btn-sm waves-effect waves-light">
                  <span class="ti-trash"></span>  Hapus
                </button>
              </form>
              @endif
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection

@section('modal')
<div class="modal fade modalAdd" role="dialog" aria-labelledby="modalAdd" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="modalAdd">Tambah Reminder</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <form action="{{ route('reminder.store') }}" id="form-add" method="POST" class="form-horizontal" autocomplete="off" role="form">
          {{ csrf_field() }}
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label class="">Progress</label>
                <select name="id_progress_detail" class="form-control select2" required>
                  <option value="">Pilih Progress</option>
                  @foreach($progresses as $progress)
                  <option value="{{ $progress->id }}">{{ $progress->progress->nama }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label for="nama">Keterangan</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="" minlength="3" maxlength="50" required>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label class="">Operator</label>
                <select name="operator" class="form-control select2" required>
                  <option value="<"><</option>
                  <option value="<="><=</option>
                  <option value=">">></option>
                  <option value=">=">>=</option>
                  <option value="==">==</option>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="amount">Lama waktu</label>
                <input type="text" class="form-control" name="amount" required>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label class="">Satuan waktu</label>
                <select name="waktu" class="form-control select2" required>
                  <option value="day">Hari</option>
                  <option value="month">Bulan</option>
                  <option value="year">Tahun</option>
                </select>
              </div>
            </div>
          </div>
          <div class="form-group">
            <button class="btn btn-primary btn-sm pull-right"><span class="ti-check"></span> Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade modalEdit" role="dialog" aria-labelledby="modalEdit" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="modalEdit">Ubah Jenis Izin</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <form action="" id="form-edit" method="POST" class="form-horizontal" autocomplete="off" role="form">
          {{ csrf_field() }}
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label class="">Progress</label>
                <select name="id_progress_detail" class="form-control select2" required>
                  <option value="">Pilih Progress</option>
                  @foreach($progresses as $progress)
                  <option value="{{ $progress->id }}">{{ $progress->progress->nama }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label for="nama">Keterangan</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="" minlength="3" maxlength="50" required>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label class="">Operator</label>
                <select name="operator" class="form-control select2" required>
                  <option value="<"><</option>
                  <option value="<="><=</option>
                  <option value=">">></option>
                  <option value=">=">>=</option>
                  <option value="==">==</option>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="amount">Lama waktu</label>
                <input type="text" class="form-control" name="amount" required>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label class="">Satuan waktu</label>
                <select name="waktu" class="form-control select2" required>
                  <option value="day">Hari</option>
                  <option value="month">Bulan</option>
                  <option value="year">Tahun</option>
                </select>
              </div>
            </div>
          </div>
          <div class="form-group">
            <button class="btn btn-primary btn-sm pull-right"><span class="ti-check"></span> Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js-bottom')
  <script>
    $(function(){

      $('.select2').select2()

      var formAdd = $('#form-add');
      var formEdit = $('#form-edit');

      var table = $('#datatable-buttons').DataTable({
        lengthChange: false,
        buttons: ['copy', 'excel', 'pdf']
      }).buttons().container().appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');

      $('#btn-add').detach().appendTo('.dt-buttons');

      $('tbody').on('click', '.btn-delete', function(event){
        event.preventDefault();
        var form = $(this).parent();

        swal({
          title: 'Anda akan menghapus data ini?',
          text: "Anda tidak dapat mengembalikan data yang telah dihapus!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#4fa7f3',
          cancelButtonColor: '#d57171',
          confirmButtonText: 'Ya'
        }).then(function (confirm) {
          if(confirm){
            form.submit();
          }
        }).catch(function(dismiss){
          //when cancel or overlay
        });
      });

      $('tbody').on('click', '.btn-edit', function(){
        const nama = $(this).data('nama');
        const hari = $(this).data('hari');
        const operator = $(this).data('operator');
        const progressDetailId = $(this).data('progress-detail');

        formEdit.find('input[name=nama]').val(nama);
        formEdit.find('input[name=amount]').val(hari);
        formEdit.find('select[name=operator]').val(operator).trigger('change');
        formEdit.find('select[name=id_progress_detail]').val(progressDetailId).trigger('change');
        formEdit.attr('action', $(this).data('route'))
      });

    });
  </script>
@endsection