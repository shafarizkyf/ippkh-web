@extends('layouts.admin')

@section('page-title')
Alur Pengajuan
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card-box table-responsive">
      @if(in_array('progress_workflow.store', Auth::user()->routePermissions()))
      <a class="btn btn-primary btn-sm waves-effect waves-light" id="btn-add" href="{{ route('progress_workflow.add') }}">
        <span class="ti-plus"></span> Tambah
      </a>
      @endif
      <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>Jenis</th>
            <th>Jumlah Tahapan</th>
            @if(in_array('progress_workflow.update', Auth::user()->routePermissions()) || in_array('progress_workflow.delete', Auth::user()->routePermissions()))
            <th></th>
            @endif
          </tr>
        </thead>
        <tbody>
          @foreach($workflows as $workflow)
          <tr>
            <td>{{ $workflow['mekanisme'] }}</td>
            <td>{{ count($workflow['alur']) }} Tahapan</td>
            <td>
              @if(in_array('progress_workflow.update', Auth::user()->routePermissions()))
              <a href="{{ route('progress_workflow.edit', $workflow['mekanisme_id']) }}" class="btn btn-warning btn-sm waves-effect waves-light"><span class="ti-eye"></span> Edit</a>
              @endif
              @if(in_array('progress_workflow.delete', Auth::user()->routePermissions()))
              <form action="{{ route('progress_workflow.delete', $workflow['mekanisme_id']) }}" class="form-delete" method="POST" style="display: inline">
                {{ csrf_field() }}
                <button class="btn btn-danger btn-delete btn-sm waves-effect waves-light">
                  <span class="ti-trash"></span>  Hapus
                </button>
              </form>
              @endif
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection

@section('js-bottom')
  <script>
    $(function(){

      var form = $('#form-add');
      form.find('.select2').select2();

      var table = $('#datatable-buttons').DataTable({
        lengthChange: false,
        buttons: ['copy', 'excel', 'pdf']
      }).buttons().container().appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');

      $('#btn-add').detach().appendTo('.dt-buttons');

      $('tbody').on('click', '.btn-delete', function(event){
        event.preventDefault();
        var form = $(this).parent();

        swal({
          title: 'Anda akan menghapus data ini?',
          text: "Anda tidak dapat mengembalikan data yang telah dihapus!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#4fa7f3',
          cancelButtonColor: '#d57171',
          confirmButtonText: 'Ya'
        }).then(function (confirm) {
          if(confirm){
            form.submit();
          }
        }).catch(function(dismiss){
          //when cancel or overlay
        });
      });

    });
  </script>
@endsection