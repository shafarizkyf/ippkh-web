@extends('layouts.admin')

@section('page-title')
  Alur Pengajuan {{ ucfirst($workflow->first()->mekanisme->nama) }}
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <div class="row">
        <div class="col-md-6">
          <h3>Alur Saat Ini</h3>
          <p class="text-muted">Anda dapat merubah urutan alur progress dibawah ini dengan "klik dan tahan" progress tertenu dan "tarik" ke urutan yang dinginkan</p>
          <ul class="list" id="sortable" data-route="{{ route('progress_workflow.update', $workflow->first()->mekanisme_id) }}">
            @foreach($workflow as $o)
            <li id="{{ $o->id }}" data-progress="{{ $o->progress_id }}" data-order="{{ $o->order }}">{{ $o->progress->nama }}</li>
            @endforeach
          </ul>
          @if(in_array('progress_workflow.update', Auth::user()->routePermissions()))
          <button class="btn btn-primary btn-block" id="btn-save">Simpan</button>
          @endif
        </div>
        <div class="col-md-6">
          <h3>Komponen Progress</h3>
          <p class="text-muted">Anda dapat menambah progress saat ini dengan "klik dan tahan" pilihan progress dibawah ini lalu "tarik" ke kolom "alur saat ini"</p>
          <ul class="list">
            @foreach($progress as $o)
            <li class="draggable" id="{{ $o->id }}" data-progress="{{ $o->id }}">{{ $o->nama }}</li>
            @endforeach
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('css-bottom')
  <style>
    .list {
      padding: 0px;
      list-style: none;
    }
    .list > li {
      border:1px solid #dee2e6;
      padding: 10px;
      margin: 10px 0;
    }
  </style>
@endsection

@section('js-bottom')
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
    $('#sortable').sortable({
      update: function( event, ui ) {
        $("#sortable li").each(function(i, element) {
          $element = $(element);
          $element.attr('data-order', $element.index("#sortable li"));
        });        
      },
      out: function () {
        removeIntent = true;
      },
      over: function () {
        removeIntent = false;
      },
      beforeStop: function (event, ui) {
        if(removeIntent === true) {
          ui.item.hide();
          ui.item.remove();
        }
      }            
    });

    $('.draggable').draggable({
      connectToSortable: "#sortable",
      helper: "clone",
      revert: "invalid",
      scroll: true,
      scrollSpeed: 100,
      scrollSensitivity: 100
    });

    $('#btn-save').click(() => {
      const route = $('#sortable').data('route')
      const workflow = []

      $('#sortable li').each((i, element) => {
        element = $(element)
        id = element.attr('id')
        order = element.data('order')
        id_progress = element.data('progress')
        workflow.push({id, order, id_progress})
      })
      
      $.post(route, {workflow}).then((response) => {
        if(response.success){
          location.reload()
        }
      })
    })
  </script>
@endsection