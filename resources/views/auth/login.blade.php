<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Monitoring Progress Izin Pinjam Pakai Kawasan Hutan">
    <meta name="author" content="shafarizkyf">
    <link rel="shortcut icon" href="{{ asset('template/assets/images/favicon.png') }}">
    <title>Aplikasi Penggunaan dan Tukar Menukar Kawasan Hutan Perum Perhutani Divisi Regional</title>
    <!-- App css -->
    <link href="{{ asset('template/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('template/assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('template/assets/css/style.css') }}" rel="stylesheet" type="text/css" />
    <script src="{{ asset('template/assets/js/modernizr.min.js') }}"></script>
  </head>
  <body>
    <div class="account-pages"></div>
    <div class="clearfix"></div>
    <div class="wrapper-page">
      <img src="/logo.png" class="mx-auto d-block">
      <div class="text-center">
        <a href="index.html" class="logo"><span>Departemen Perencanaan dan Pengembangan Bisnis<span></span></span></a>
        <h5 class="text-muted m-t-0 font-600">Sistem Informasi Pengecekan Penggunaan Kawasan Hutan
          Perum Perhutani Divisi Regional Jawa Tengah</h5>
      </div>
      <div class="m-t-40 card-box">
        <div class="text-center">
          <h4 class="text-uppercase font-bold m-b-0">Masuk</h4>
          @if(Session::has('error'))
          <span class="text-danger">{{ Session::get('error') }}</span>
          @endif
        </div>
        <div class="p-20">
          <form class="form-horizontal m-t-20" action="{{ route('login') }}" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
              <div class="col-xs-12">
                <input class="form-control" name="username" type="text" required="" placeholder="Username">
              </div>
            </div>
            <div class="form-group">
              <div class="col-xs-12">
                <input class="form-control" name="password" type="password" required="" placeholder="Password">
              </div>
            </div>
            <div class="form-group text-center m-t-30">
              <div class="col-xs-12">
                <button class="btn btn-custom btn-bordred btn-block waves-effect waves-light" type="submit">Login</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- end wrapper page -->
    <!-- jQuery  -->
    <script src="{{ asset('template/assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('template/assets/js/popper.min.js') }}"></script>
    <script src="{{ asset('template/assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('template/assets/js/detect.js') }}"></script>
    <script src="{{ asset('template/assets/js/fastclick.js') }}"></script>
    <script src="{{ asset('template/assets/js/jquery.blockUI.js') }}"></script>
    <script src="{{ asset('template/assets/js/waves.js') }}"></script>
    <script src="{{ asset('template/assets/js/jquery.nicescroll.js') }}"></script>
    <script src="{{ asset('template/assets/js/jquery.slimscroll.js') }}"></script>
    <script src="{{ asset('template/assets/js/jquery.scrollTo.min.js') }}"></script>
    <!-- App js -->
    <script src="{{ asset('template/assets/js/jquery.core.js') }}"></script>
    <script src="{{ asset('template/assets/js/jquery.app.js') }}"></script>
  </body>
</html>