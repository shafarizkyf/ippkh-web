<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Monitoring Progress Izin Pinjam Pakai Kawasan Hutan">
    <meta name="author" content="shafarizkyf">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ asset('template/assets/images/favicon.png') }}">
    <title>Aplikasi Penggunaan dan Tukar Menukar Kawasan Hutan Perum Perhutani Divisi Regional</title>
    @yield('css-top')
    <!-- Sweet Alert css -->
    <link href="{{ asset('template/assets/plugins/sweet-alert/sweetalert2.min.css') }}"  rel="stylesheet" type="text/css" />
    <!-- DataTables -->
    <link href="{{ asset('template/assets/plugins/datatables/dataTables.bootstrap4.min.css') }}"  rel="stylesheet" type="text/css" />
    <link href="{{ asset('template/assets/plugins/datatables/buttons.bootstrap4.min.css') }}"  rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="{{ asset('template/assets/plugins/datatables/responsive.bootstrap4.min.css') }}"  rel="stylesheet" type="text/css" />
    <!-- Select2 css -->
    <link href="{{ asset('template/assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- App css -->
    <link href="{{ asset('template/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('template/assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('template/assets/css/style.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/mystyles.css') }}" rel="stylesheet" type="text/css" />
    @yield('css-bottom')
    <script src="{{ asset('template/assets/js/modernizr.min.js') }}"></script>
  </head>
  <body>
    <!-- Begin page -->
    <div id="wrapper">
      <!-- Top Bar Start -->
      <div class="topbar">
        <!-- LOGO -->
        <div class="topbar-left">
          <a href="{{ route('penggunaan.index') }}" class="logo"><span>Monitor<span>ing</span></span><i class="mdi mdi-layers"></i></a>
        </div>
        <!-- Button mobile view to collapse sidebar menu -->
        <div class="navbar navbar-default" role="navigation">
          <div class="container-fluid">
            <!-- Page title -->
            <ul class="nav navbar-nav list-inline navbar-left">
              <li class="list-inline-item">
                <button class="button-menu-mobile open-left">
                <i class="mdi mdi-menu"></i>
                </button>
              </li>
              <li class="list-inline-item">
                <h4 class="page-title">@yield('page-title')</h4>
              </li>
            </ul>
          </div>
          <!-- end container -->
        </div>
        <!-- end navbar -->
      </div>
      <!-- Top Bar End -->
      <!-- ========== Left Sidebar Start ========== -->
      <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">
          <!-- User -->
          <div class="user-box">
            <div class="user-img">
              <img src="{{ asset('template/assets/images/users/avatar-1.png') }}" alt="user-img" class="rounded-circle img-thumbnail img-responsive">
            </div>
            <h5>{{ Auth::user()->nama }}</h5>
            <ul class="list-inline">
              <li class="list-inline-item">
                <a href="{{ route('logout') }}" class="text-custom">
                <i class="mdi mdi-power"></i>
                </a>
              </li>
            </ul>
          </div>
          <!-- End User -->
          <!--- Sidemenu -->
          <div id="sidebar-menu">
            <ul>
              <li class="text-muted menu-title">Navigasi</li>
              <li>
                <a href="{{ route('dashboard') }}" class="waves-effect"><i class="ti-dashboard"></i> <span> Dashboard </span> </a>
              </li>
              @if(in_array('penggunaan.index', Auth::user()->routePermissions()) || in_array('progress_workflow.index', Auth::user()->routePermissions()))
              <li class="has_sub">
                <a href="javascript:void(0);" class="waves-effect"><i class="ti-notepad"></i> <span>Penggunaan </span> <span class=""></span></a>
                <ul class="list-unstyled">
                  @if(in_array('penggunaan.index', Auth::user()->routePermissions()))
                  <li><a href="{{ route('penggunaan.index') }}">Pengajuan</a></li>
                  @endif
                  @if(in_array('progress_workflow.index', Auth::user()->routePermissions()))
                  <li><a href="{{ route('progress_workflow.index') }}">Alur Pengajuan</a></li>
                  @endif
                  @if(in_array('progress.index', Auth::user()->routePermissions()))
                  <li><a href="{{ route('progress.index') }}">Komponen Progress</a></li>
                  @endif
                </ul>
              </li>
              @endif
              @if(0)
              <li class="has_sub">
                <a href="javascript:void(0);" class="waves-effect"><i class="ti-book"></i> <span>Pemanfaatan </span> <span class=""></span></a>
                <ul class="list-unstyled">
                  <li><a href="#">Pengajuan</a></li>
                  <li><a href="#">Management Approval</a></li>
                </ul>
              </li>
              @endif
              @if(in_array('pengguna.index', Auth::user()->routePermissions()) || in_array('role.index', Auth::user()->routePermissions()))
              <li class="has_sub">
                <a href="javascript:void(0);" class="waves-effect"><i class="ti-user"></i> <span>Pengguna </span> <span class=""></span></a>
                <ul class="list-unstyled">
                  @if(in_array('pengguna.index', Auth::user()->routePermissions()))
                  <li><a href="{{ route('pengguna.index') }}">Pengguna</a></li>
                  @endif
                  @if(in_array('role.index', Auth::user()->routePermissions()))
                  <li><a href="{{ route('role.index') }}">Role</a></li>
                  @endif
                </ul>
              </li>
              @endif
              @if(in_array('bidang-kegiatan.index', Auth::user()->routePermissions()) ||
                  in_array('jenis-izin.index', Auth::user()->routePermissions()) ||
                  in_array('jenis-kegiatan.index', Auth::user()->routePermissions()) ||
                  in_array('jenis-pemohon.index', Auth::user()->routePermissions()) ||
                  in_array('mekanisme.index', Auth::user()->routePermissions()))
              <li class="has_sub">
                <a href="javascript:void(0);" class="waves-effect"><i class="ti-book"></i> <span>Master Data </span> <span class=""></span></a>
                <ul class="list-unstyled">
                  @if(in_array('jenis-kegiatan.index', Auth::user()->routePermissions()))
                  <li><a href="{{ route('jenis-kegiatan.index') }}">Jenis Kegiatan</a></li>
                  @endif
                  @if(in_array('bidang-kegiatan.index', Auth::user()->routePermissions()))
                  <li><a href="{{ route('bidang-kegiatan.index') }}">Bidang Kegiatan</a></li>
                  @endif
                  @if(in_array('jenis-pemohon.index', Auth::user()->routePermissions()))
                  <li><a href="{{ route('jenis-pemohon.index') }}">Jenis Pemohon</a></li>
                  @endif
                  @if(in_array('mekanisme.index', Auth::user()->routePermissions()))
                  <li><a href="{{ route('mekanisme.index') }}">Mekanisme</a></li>
                  @endif
                  @if(in_array('jenis-izin.index', Auth::user()->routePermissions()))
                  <li><a href="{{ route('jenis-izin.index') }}">Jenis Izin</a></li>
                  @endif
                </ul>
              </li>
              @endif
              @if(in_array('reminder.index', Auth::user()->routePermissions()) || in_array('shortcut.add', Auth::user()->routePermissions()))
              <li class="has_sub">
                <a href="javascript:void(0);" class="waves-effect"><i class="ti-settings"></i> <span>Pengaturan </span> <span class=""></span></a>
                <ul class="list-unstyled">
                  @if(in_array('reminder.index', Auth::user()->routePermissions()))
                  <li><a href="{{ route('reminder.index') }}">Pengingat</a></li>
                  @endif
                  @if(in_array('shortcut.add', Auth::user()->routePermissions()))
                  <li><a href="{{ route('shortcut.add') }}">Shortcut</a></li>
                  @endif
                </ul>
              </li>
              @endif
            </ul>
            <div class="clearfix"></div>
          </div>
          <!-- Sidebar -->
          <div class="clearfix"></div>
        </div>
      </div>
      <!-- Left Sidebar End -->
      <!-- ============================================================== -->
      <!-- Start right Content here -->
      <!-- ============================================================== -->
      <div class="content-page">
        <!-- Start content -->
        <div class="content">
          <div class="container-fluid">
            @if(Session::has('success'))
            <div class="alert alert-success">
              <strong>Berhasil!</strong> {{ Session::get('success') }}
            </div>
            @elseif(Session::has('warning'))
            <div class="alert alert-success">
              <strong>Peringatan!</strong> {{ Session::get('warning') }}
            </div>
            @elseif(Session::has('fail'))
            <div class="alert alert-success">
              <strong>Gagal!</strong> {{ Session::get('fail') }}
            </div>
            @endif

            @yield('content')
          </div>
          <!-- container -->
        </div>
        <!-- content -->
        <footer class="footer text-right">
          Perum Perhutani
        </footer>
      </div>
      @yield('modal')
      <!-- ============================================================== -->
      <!-- End Right content here -->
      <!-- ============================================================== -->
    </div>
    <!-- END wrapper -->
    <!-- jQuery  -->
    <script src="{{ asset('template/assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('template/assets/js/popper.min.js') }}"></script>
    <script src="{{ asset('template/assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('template/assets/js/detect.js') }}"></script>
    <script src="{{ asset('template/assets/js/fastclick.js') }}"></script>
    <script src="{{ asset('template/assets/js/jquery.blockUI.js') }}"></script>
    <script src="{{ asset('template/assets/js/waves.js') }}"></script>
    <script src="{{ asset('template/assets/js/jquery.nicescroll.js') }}"></script>
    <script src="{{ asset('template/assets/js/jquery.slimscroll.js') }}"></script>
    <script src="{{ asset('template/assets/js/jquery.scrollTo.min.js') }}"></script>
    <!-- App js -->
    <script src="{{ asset('template/assets/js/jquery.core.js') }}"></script>
    <script src="{{ asset('template/assets/js/jquery.app.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.repeater/1.2.1/jquery.repeater.min.js"></script>
    <!-- Sweet Alert Js  -->
    <script src="{{ asset('template/assets/plugins/sweet-alert/sweetalert2.min.js') }}" type="text/javascript"></script>
    <!-- Required datatable js -->
    <script src="{{ asset('template/assets/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('template/assets/plugins/datatables/dataTables.bootstrap4.min.js') }}" type="text/javascript"></script>
    <!-- Buttons examples -->
    <script src="{{ asset('template/assets/plugins/datatables/dataTables.buttons.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('template/assets/plugins/datatables/buttons.bootstrap4.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('template/assets/plugins/datatables/jszip.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('template/assets/plugins/datatables/pdfmake.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('template/assets/plugins/datatables/vfs_fonts.js') }}" type="text/javascript"></script>
    <script src="{{ asset('template/assets/plugins/datatables/buttons.html5.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('template/assets/plugins/datatables/buttons.print.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('template/assets/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script>
    @yield('js-bottom')
  </body>
</html>