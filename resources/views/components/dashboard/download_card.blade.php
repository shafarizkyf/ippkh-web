<a target="_blank" href="{{ $route ?? '' }}" style="color: #343a40">
  <div class="card-box h-100">
    <h4 class="header-title mt-0 m-b-30">Alur Proses Perizinan</h4>
    <div class="widget-box-2">
      <div class="widget-detail-2">
        <h4 class="mb-0">{{ $title ?? 'Title' }}</h4>
      </div>
    </div>
  </div>
</a>