<a href="{{ $route ?? '#' }}">
  <div class="card-box h-100 {{ $color ?? '' }} text-white">
    <h4 class="header-title mt-0 m-b-30">{{ $title ?? 'Title' }}</h4>
    <div class="widget-box-2">
      <div class="widget-detail-2">
        <h2 class="mb-0">{{ $amount ?? '00' }}</h2>
        <p class="m-0">{{ $subtitle ?? '' }}</p>
      </div>
    </div>
  </div>
</a>