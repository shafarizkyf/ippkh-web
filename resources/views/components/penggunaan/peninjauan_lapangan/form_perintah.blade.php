<div class="modal fade modal-form-perintah" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myLargeModalLabel">Surat Perintah Peninjauan Lapangan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-xl-12">
            <form action="{{ route('penggunaan.peninjauan.surat-perintah.store', $pengajuan->id) }}" method="POST" enctype="multipart/form-data" role="form">
              {{ csrf_field() }}
              <div class="form-group">
                <label for="nomor">Nomor Surat</label>
                <input type="text" name="nomor" id="nomor" class="form-control" placeholder="Nomor Surat" required>
              </div>
              <div class="form-group">
                <label for="surat">File Surat Undangan</label>
                <input type="file" name="surat" id="surat" class="form-control" accept="application/pdf" required>
              </div>
              <button class="btn btn-primary pull-right">Simpan</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

