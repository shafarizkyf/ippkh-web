<div class="modal fade modal-form-tagihan" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myLargeModalLabel">Tagihan Biaya Peninjauan Lapangan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-xl-12">
            <form action="{{ route('penggunaan.peninjauan.tagihan.store', $pengajuan->id) }}" method="POST" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="row">
                <div class="col-xl-6">
                  <div class="form-group">
                    <label for="nomor">Nomor Surat</label>
                    <input type="text" name="nomor" id="nomor" class="form-control" placeholder="Nomor Surat" required>
                  </div>
                  <div class="form-group">
                    <label for="surat">File Surat Undangan</label>
                    <input type="file" name="surat" id="surat" class="form-control" accept="application/pdf" required>
                  </div>
                </div>
                <div class="col-xl-6">
                  <div class="form-group">
                    <label for="biaya">Biaya Pemeriksaan Lapangan</label>
                    <input type="text" name="biaya" id="biaya" class="form-control" placeholder="Biaya" required>
                  </div>                  
                  <div class="form-group">
                    <label for="tanggal_tenggang">Harus dibayarkan sebelum</label>
                    <input type="text" name="tanggal_tenggang" id="tanggal_tenggang" class="form-control datepicker" placeholder="Tanggal harus bayar" required>
                  </div>
                </div>
              </div>
              <button class="btn btn-primary pull-right">Simpan</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

