<div class="modal fade modal-form-undangan" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myLargeModalLabel">Tanggal Peninjauan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-xl-12">
            <form action="{{ route('penggunaan.peninjauan.undangan.store', $pengajuan->id) }}" method="POST" enctype="multipart/form-data" role="form">
              {{ csrf_field() }}
              <div class="row m-b-10">
                <div class="col-xl-6">
                  <div class="form-group">
                    <label for="nomor">Nomor Surat</label>
                    <input type="text" name="nomor" id="nomor" class="form-control" placeholder="Nomor Surat" required>
                  </div>
                  <div class="form-group">
                    <label for="surat">File Surat Undangan</label>
                    <input type="file" name="surat" id="surat" class="form-control" accept="application/pdf" required>
                  </div>
                </div>
                <div class="col-xl-6">
                  @foreach($pengajuan->wilayah as $wilayah)
                  <div class="form-group">
                    <label for="kph">Pilih KPH</label>
                    <select name="id_kph[]" id="kph" class="form-control select2" required readonly>
                      <option value="{{ $wilayah->kph->id }}">{{ $wilayah->kph->nama }}</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="tanggal">Tanggal Mulai Peninjauan</label>
                    <input type="text" class="form-control datepicker" name="tanggal_mulai[]" placeholder="Tanggal Mulai" required>
                  </div>
                  <div class="form-group">
                    <label for="tanggal">Tanggal Selesai Peninjauan</label>
                    <input type="text" class="form-control datepicker" name="tanggal_selesai[]" placeholder="Tanggal Selesai" required>
                  </div>
                  @endforeach
                </div>                
              </div>
              <button class="btn btn-primary">Simpan</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

