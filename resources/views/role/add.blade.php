@extends('layouts.admin')

@section('page-title')
Tambah Role
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <form action="{{ route('role.store') }}" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
          <label for="nama">Nama Role</label>
          <input type="text" name="nama" class="form-control" placeholder="Nama Role">
        </div>
        <div class="row">
          @foreach($moduls as $modul)
          <div class="col-md-4">
            <h4>{{ $modul->nama }}</h4>
            @foreach($modul->permissions as $permission)
            <div class="checkbox checkbox-success">
              <input id="{{ $permission->id }}" name="permissions[]" type="checkbox" value="{{ $permission->id }}">
              <label for="{{ $permission->id }}">{{ $permission->nama }}</label>
            </div>
            @endforeach
          </div>
          @endforeach
        </div>
        <div class="form-group text-right">
          <button class="btn btn-primary btn-sm"><span class="ti-check"></span> Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
