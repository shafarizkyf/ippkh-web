@extends('layouts.admin')

@section('page-title')
List Role
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card-box table-responsive">
      @if(in_array('role.store', Auth::user()->routePermissions()))
      <a class="btn btn-primary btn-sm waves-effect waves-light" id="btn-add" href="{{ route('role.create') }}">
        <span class="ti-plus"></span> Tambah
      </a>
      @endif
      <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>Role</th>
            <th>Permissions</th>
            @if(in_array('role.update', Auth::user()->routePermissions()) || in_array('role.delete', Auth::user()->routePermissions()))
            <th></th>
            @endif
          </tr>
        </thead>
        <tbody>
          @foreach($roles as $role)
          <tr>
            <td>{{ $role->nama }}</td>
            <td>{{ $role->rolePermissions->count() }} Permissions</td>
            <td>
              @if(in_array('role.update', Auth::user()->routePermissions()))
              <a href="{{ route('role.edit', $role->id) }}" class="btn btn-warning btn-sm waves-effect waves-light"><span class="ti-eye"></span> Edit</a>
              @endif
              @if(in_array('role.delete', Auth::user()->routePermissions()))
              <form action="{{ route('role.delete', $role->id) }}" class="form-delete" method="POST" style="display: inline">
                {{ csrf_field() }}
                <button class="btn btn-danger btn-delete btn-sm waves-effect waves-light">
                  <span class="ti-trash"></span>  Hapus
                </button>
              </form>
              @endif
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection

@section('js-bottom')
  <script>
    $(function(){

      var form = $('#form-add');
      form.find('.select2').select2();

      var table = $('#datatable-buttons').DataTable({
        lengthChange: false,
        buttons: ['copy', 'excel', 'pdf']
      }).buttons().container().appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');

      $('#btn-add').detach().appendTo('.dt-buttons');

      $('.btn-delete').on('click', function(event){
        event.preventDefault();
        var form = $(this).parent();

        swal({
          title: 'Anda akan menghapus data ini?',
          text: "Anda tidak dapat mengembalikan data yang telah dihapus!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#4fa7f3',
          cancelButtonColor: '#d57171',
          confirmButtonText: 'Ya'
        }).then(function (confirm) {
          if(confirm){
            form.submit();
          }
        }).catch(function(dismiss){
          //when cancel or overlay
        });
      });

    });
  </script>
@endsection