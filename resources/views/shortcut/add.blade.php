@extends('layouts.admin')

@section('page-title')
  Tambah Dashboard Shortcut
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <div class="row">
        <div class="col-md-6">
          <h3>Buat Shortcut</h3>
          <p class="text-muted">Anda dapat merubah urutan shortcut dibawah ini dengan "klik dan tahan" progress tertenu dan "tarik" ke urutan yang dinginkan</p>
          <ul class="list" id="sortable" data-route="{{ route('shortcut.store') }}" style="min-height: 100px">
            @foreach($shortcuts as $o)
            <li id="{{ $o->id }}" data-progress="{{ $o->progress_id }}" data-order="{{ $o->order }}">{{ $o->progress->nama }}</li>
            @endforeach
          </ul>
          @if(in_array('progress_workflow.update', Auth::user()->routePermissions()))
          <button class="btn btn-primary btn-block" id="btn-save">Simpan</button>
          @endif
        </div>
        <div class="col-md-6">
          <h3>Komponen Progress</h3>
          <p class="text-muted">Anda dapat menambah shortcut saat ini dengan "klik dan tahan" pilihan progress dibawah ini lalu "tarik" ke kolom "shortcut"</p>
          <ul class="list">
            @foreach($progresses as $o)
            <li class="draggable" id="{{ $o->id }}" data-progress="{{ $o->id }}">{{ $o->nama }}</li>
            @endforeach
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('css-bottom')
  <style>
    .list {
      padding: 0px;
      list-style: none;
    }
    .list > li {
      border:1px solid #dee2e6;
      padding: 10px;
      margin: 10px 0;
    }
  </style>
@endsection

@section('js-bottom')
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
    $('.select2').select2();

    $('#sortable').sortable({
      update: function( event, ui ) {
        $("#sortable li").each(function(i, element) {
          $element = $(element);
          $element.attr('data-order', $element.index("#sortable li"));
        });
      },
      out: function () {
        removeIntent = true;
      },
      over: function () {
        removeIntent = false;
      },
      beforeStop: function (event, ui) {
        if(removeIntent === true) {
          ui.item.hide();
          ui.item.remove();
        }
      }
    });

    $('.draggable').draggable({
      connectToSortable: "#sortable",
      helper: "clone",
      revert: "invalid",
      scroll: true,
      scrollSpeed: 100,
      scrollSensitivity: 50
    });

    $('#btn-save').click(() => {
      const route = $('#sortable').data('route');
      const shortcut = []

      $('#sortable li').each((i, element) => {
        element = $(element)
        id = element.attr('id')
        order = element.data('order')
        id_progress = element.data('progress')
        shortcut.push({id, order, id_progress})
      })

      const data = { shortcut }

      if(shortcut.length == 0){
        alert('Anda belum menetapkan shortcut');
        return;
      }

      $.ajax({
        url: route,
        method: 'POST',
        data,
        headers: {
          'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')
        }
      }).then((response) => {
        if(response.success){
          alert('Berhasil menyimpan shortcut');
          location.reload()
        }else{
          alert(response.message);
        }
      })
    })
  </script>
@endsection