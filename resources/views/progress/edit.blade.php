@extends('layouts.admin')

@section('page-title')
Ubah Format Komponen
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="card-box">
        <div class="row">
          <div class="col-md-8 offset-2">
            <h4>Progress {{ $progress->nama }}</h4>
            <form action="{{ route('progress.update', $progress->id) }}" method="POST" class="repeater">
              {{ csrf_field() }}
              <div data-repeater-list="keterangan">
                @foreach($progress->progressDetail as $detail)
                <div data-repeater-item>
                  <div class="row">
                    <div class="col-md-10">
                      <div class="form-group">
                        <select name="keterangan_id" class="form-control">
                          <option value="">Pilih Jenis Kolom</option>
                          @foreach($keterangan as $o)
                          <option value="{{ $o->id }}" {{ $o->id == $detail->keterangan_id ? 'selected' : '' }}>{{ $o->nama }}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <button class="btn btn-danger" data-repeater-delete type="button">Hapus</button>
                      </div>
                    </div>
                  </div>
                </div>
                @endforeach
              </div>
              <button class="btn btn-primary">Simpan</button>
              <button class="btn btn-secondary" data-repeater-create type="button">Tambah</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('js-bottom')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.repeater/1.2.1/jquery.repeater.min.js"></script>
  <script>
    $(function(){
      $('.repeater').repeater()
    })
  </script>
@endsection
