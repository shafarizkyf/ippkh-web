@extends('layouts.admin')

@section('page-title')
  List Pengajuan Penggunaan
@endsection

@section('content')
  <div class="row">
    <div class="col-12">
      <div class="card-box">
        <div class="table-rep-plugin">
          <div class="table-responsive" data-pattern="priority-columns">
            @if(in_array('penggunaan.store', Auth::user()->routePermissions()))
            <div class="btn-group m-l-5 m-b-5" id="btn-add">
              <a class="btn btn-success waves-effect waves-light" href="{{ route('penggunaan.create') }}">
                <span class="ti-plus"></span> Tambah
              </a>
            </div>  
            @endif
            @if(in_array('penggunaan.index', Auth::user()->routePermissions()))   
            <div class="btn-group m-l-5 m-b-5" id="btn-filter">
              <a class="btn btn-secondary waves-effect waves-light text-white" data-toggle="modal" data-target=".bs-example-modal-lg">
                <span class="ti-filter"></span> Filter
              </a>
            </div>
            @endif
            <table id="tech-companies-1" class="table table-striped">
              <thead>
                <tr>
                  <th data-priority="1">Pemohon</th>
                  <th data-priority="2">Kegiatan</th>
                  <th data-priority="3">Mekanisme</th>
                  <th data-priority="4">KPH</th>
                  <th data-priority="7">Progress Terbaru</th>
                </tr>
              </thead>
              <tbody>
                @foreach($pengajuan as $o)
                <tr>
                  @if(in_array('penggunaan.detail', Auth::user()->routePermissions()))
                  <td class="wrap160"><a class="wrap160" href="{{ route('penggunaan.detail', $o->id) }}">{{ $o->pemohon->nama }}</a></td>
                  @else
                  <td class="wrap160">{{ $o->pemohon->nama }}</td>
                  @endif
                  <td class="wrap160"><span class="wrap160">{{ $o->kegiatan }}</span></td>
                  <td class="wrap160">{{ $o->mekanisme->nama }}</td>
                  <td class="wrap160">
                    @foreach ($o->wilayah as $w)
                      <span>{{ $w->kph->nama }}</span><br>
                    @endforeach
                  </td>
                  <td class="wrap160">
                    @if($o->progressPengajuan->count())
                    <span>{{ $o->progressPengajuan->last()->progress->nama }}</span><br>
                    @endif
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          {{ $pengajuan->links() }}
        </div>
      </div>
    </div>
  </div>
@endsection

@section('modal')
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myLargeModalLabel">Filter Pengajuan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-xl-12">
            <form action="{{ route('penggunaan.index') }}" id="form-filter" method="GET" class="form-horizontal" role="form">
              <div class="row">
                <div class="col-xl-6">
                  <div class="form-group">
                    <label class="col-form-label">Pemohon</label>
                    <input type="text" class="form-control" name="pemohon" placeholder="Nama Instansi/Perusahaan">
                  </div>        
                  <div class="form-group">
                    <label class="col-form-label">NPWP</label>
                    <input type="text" class="form-control" name="npwp" placeholder="NPWP">
                  </div>
                  <div class="form-group">
                    <label class="col-form-label">Jenis Pemohon</label>
                    <select name="id_jenis_pemohon" class="form-control select2">
                      <option value="">Pilih Jenis Pemohon</option>
                      @foreach ($jenisPemohon as $o)
                        <option value="{{ $o->id }}">{{ $o->nama }}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label class="col-form-label">Kegiatan</label>
                    <input type="text" class="form-control" name="kegiatan" placeholder="Kegiatan">
                  </div>   
                  <div class="form-group">
                    <label class="col-form-label">Nomor Surat</label>
                    <input type="text" class="form-control" name="nosurat" placeholder="Nomor Surat">
                  </div>          
                  <div class="form-group">
                    <label class="col-form-label">Mekanisme</label>
                    <select name="id_mekanisme" class="form-control select2">
                      <option value="">Pilih Mekanisme</option>
                      @foreach ($mekanisme as $o)
                        <option value="{{ $o->id }}">{{ $o->nama }}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <div class="checkbox checkbox-success">
                      <input id="checkbox-10" type="checkbox" name="show_as_laporan">
                      <label for="checkbox-10">Tampilkan dalam bentuk cetak laporan</label>
                    </div>
                  </div>
                </div>
                <div class="col-xl-6">
                  <div class="form-group">
                    <label class="col-form-label">Jenis Izin</label>
                    <select name="id_jenis_izin" class="form-control select2">
                      <option value="">Pilih Jenis Izin</option>
                      @foreach ($jenisIzin as $o)
                        <option value="{{ $o->id }}">{{ $o->nama }}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label class="col-form-label">Bidang Kegiatan</label>
                    <select name="id_bidang_kegiatan" id="bidang_kegiatan" class="form-control select2" data-route="{{ route('jenis-kegiatan.index') }}">
                      <option value="">Pilih Bidang Kegiatan</option>
                      @foreach ($bidangKegiatan as $o)
                        <option value="{{ $o->id }}">{{ $o->nama }}</option>
                      @endforeach                      
                    </select>
                  </div>
                  <div class="form-group">
                    <label class="col-form-label">Jenis Kegiatan</label>
                    <select name="id_jenis_kegiatan" id="jenis_kegiatan" class="form-control select2">
                      <option value="">Pilih Jenis Kegiatan</option>
                    </select>
                  </div>     
                  <div class="form-group">
                    <label class="col-form-label">Unit Kerja</label>
                    <select name="id_unit_kerja" id="unit_kerja" class="form-control select2" data-route="{{ route('kph.index') }}">
                      <option value="">Pilih Unit Kerja</option>
                      @foreach ($unitKerja as $o)
                        <option value="{{ $o->id }}">{{ $o->nama }}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label class="col-form-label">KPH</label>
                    <select name="id_kph" id="kph" class="form-control select2">
                      <option value="">Pilih KPH</option>
                    </select>
                  </div>                     
                  <div class="form-group">
                    <label class="col-form-label">Progress</label>
                    <select name="id_progress" class="form-control select2">
                      <option value="">Pilih Progress</option>
                      @foreach ($progress as $o)
                        <option value="{{ $o->id }}">{{ $o->nama }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
              <button class="btn btn-secondary pull-right"><span class="ti-filter"></span> Filter</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
@endsection

@section('css-top')
  <link href="{{ asset('template/assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('js-bottom')
  <script src="{{ asset('template/assets/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('template/assets/plugins/responsive-table/js/rwd-table.min.js') }}" type="text/javascript"></script>
  <script>
    $(function(){
      $('.select2').select2();
      $('.table-responsive').responsiveTable({
        addDisplayAllBtn: 'btn btn-secondary',
        i18n: {
          focus: 'Fokus', 
          display: 'Kolom', 
          displayAll: 'Semua Kolom' 
        },
        addFocusBtn :false
      });

      $('#bidang_kegiatan').on('change', function(){
        var id_bidang_kegiatan = $(this).val();
        var route = $(this).data('route');
        var jenisKegiatanEl = $('#jenis_kegiatan').find('option').remove().end().append('<option>Pilih Jenis Kegiatan</option>');

        $.getJSON(route, {id_bidang_kegiatan}).done(function(data){
          $.each(data, function(index, value){
            var newOption = new Option(value.nama, value.id, false, false);
            jenisKegiatanEl.append(newOption).trigger('change');
          });//end each
        });//end getJSON
      });

      $('#unit_kerja').on('change', function(){
        var id_unit_kerja = $(this).val();
        var route = $(this).data('route');
        var kphEl = $('#kph').find('option').remove().end().append('<option>Pilih KPH</option>');

        $.getJSON(route, {id_unit_kerja}).done(function(data){
          $.each(data, function(index, value){
            var newOption = new Option(value.nama, value.id, false, false);
            kphEl.append(newOption).trigger('change');
          });//end each
        });//end getJSON      
      });
      
      $('#btn-add').detach().appendTo('.btn-toolbar');
      $('#btn-filter').detach().appendTo('.btn-toolbar');
    })
  </script>
@endsection
