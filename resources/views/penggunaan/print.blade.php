<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Print View</title>
  <style>
    @page {
      size: landscape;
      margin: 3mm;
    }

    @media print {
      html, body {
        height: 210mm;
        width: 297mm;
      }
    }

    body{
      font-family: Arial, Helvetica, sans-serif;
    }
    h5,p{
      margin: 0;
      padding: 0;
    }
    table{
      margin-top: 10px;
      border-collapse: collapse;
    }
    th, td{
      padding: 2px 5px;
    }
    .text-center{
      text-align: center;
    }
    .f12{
      font-size: 12px;
    }
    .f8{
      font-size: 8px;
    }
    .span-block{
      display:block; 
      margin: 5px 0;
    }
  </style>
</head>
<body>
  <h5 class="text-center">REKAP PENGGUNAAN KAWASAN HUTAN DIVISI REGIONAL JAWA TENGAH</h5>
  <table border="1" align="center">
    <thead>
      <tr>
        <th class="f12">No</th>
        <th class="f12">Pemohon</th>
        <th class="f12">Lembaga Pemohon</th>
        <th class="f12">Bidang Kegiatan</th>
        <th class="f12">Detail Bidang</th>
        <th class="f12">Mekanisme</th>
        <th class="f12">Provinsi</th>
        <th class="f12">Divre</th>
        <th class="f12">KPH</th>
        <th class="f12">Luas</th>
        <th class="f12">Progress Terakhir</th>
      </tr>
      <tr>
        @for($i=1; $i<=11; $i++)
        <th class="f8">{{ $i }}</th>
        @endfor
      </tr>
    </thead>
    <tbody>
      @php $no=1 @endphp
      @foreach($pengajuan as $o)
      <tr>
        <td class="f12 text-center">{{ $no++ }}</td>
        <td class="f12">{{ $o->pemohon->nama }}</td>
        <td class="f12">{{ $o->pemohon->jenisPemohon->nama }}</td>
        <td class="f12">{{ $o->jenisKegiatan->nama }}</td>
        <td class="f12 w160">{{ $o->kegiatan }}</td>
        <td class="f12">{{ $o->mekanisme->nama }}</td>
        <td class="f12">
          @foreach($o->wilayah as $wilayah)
          <span class="span-block">{{ $wilayah->provinsi->nama }}</span>
          @endforeach
        </td>
        <td class="f12">
          @foreach($o->wilayah as $wilayah)
          <span class="span-block">{{ $wilayah->kph->unitKerja->nama }}</span>
          @endforeach
        </td>
        <td class="f12">
          @foreach($o->wilayah as $wilayah)
          <span class="span-block">{{ $wilayah->kph->nama }}</span>
          @endforeach
        </td>
        <td class="f12">
          @foreach($o->wilayah as $wilayah)
          <span class="span-block">{{ $wilayah->luas }} Ha</span>
          @endforeach
        </td>
        <td class="f12">
          @if($o->progressPengajuan->count())
          <span>{{ $o->progressPengajuan->last()->progress->nama }}</span><br>
          @else 
          <span>Belum ada catatan</span>
          @endif
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  <script>
    window.print();
  </script>
</body>
</html>