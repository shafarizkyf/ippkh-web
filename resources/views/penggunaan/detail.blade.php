@extends('layouts.admin')

@section('page-title')
  Detail Pengajuan
@endsection

@section('content')
<style type="text/css">
  .divCircle{
    cursor: pointer;
  }
  .pointer{
    cursor: pointer;
  }
  .bgsalmon{
    background-color: salmon !important
  }
</style>
<div class="row">
  <div class="col-xl-12">
    <div class="card-box">
      <h4 class="header-title m-t-0 m-b-30">Detail Pengajuan Pemohon</h4>
      <ul class="nav nav-tabs">
        <li class="nav-item">
          <a href="#one" data-toggle="tab" aria-expanded="false" class="nav-link active">Data Pemohon</a>
        </li>
        <li class="nav-item">
          <a href="#two" data-toggle="tab" aria-expanded="true" class="nav-link">Progress</a>
        </li>
        @if(in_array('penggunaan.simpanProgressPengajuan', Auth::user()->routePermissions()))
        <li class="nav-item">
          <a href="#three" data-toggle="tab" aria-expanded="false" class="nav-link">Tambah Progress</a>
        </li>
        @endif
      </ul>
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade show active" id="one">
          <div class="row">
            <div class="col-xl-12">
              <h6 class="text-muted text-center">Pemohon
                @if(in_array('penggunaan.editProgressPengajuan', Auth::user()->routePermissions()))
                  <br><a href="{{ route('penggunaan.edit', $pengajuan->id) }}">[Ubah Data Pemohon]</a></h6>
                @endif
                @if(in_array('penggunaan.delete', Auth::user()->routePermissions()))
                  <form action="{{ route('penggunaan.delete', $pengajuan->id) }}" method="POST">
                    {{ csrf_field() }}
                    <button class="btn btn-sm btn-danger pull-right" onclick="return confirm('Anda akan menghapus pengajuan ini?')">Hapus</button>
                  </form>
                @endif
              <h4 class="text-center font-bold">{{ strtoupper($pengajuan->pemohon->nama) }}</h4>
              <h5 class="text-center">NPWP. {{ $pengajuan->pemohon->npwp }}</h5>
              <hr>
              <h6 class="text-muted text-center">Pengajuan Kegiatan</h6>
              <h4 class="text-center font-bold">{{ $pengajuan->kegiatan }}</h4>
              <dl class="row m-t-20">
                <dt class="col-sm-3 text-muted">Bidang Kegiatan</dt>
                <dd class="col-sm-9">{{ $pengajuan->jenisKegiatan->bidangKegiatan->nama }}</dd>
                <dt class="col-sm-3 text-muted">Jenis Kegiatan</dt>
                <dd class="col-sm-9">{{ $pengajuan->jenisKegiatan->nama }}</dd>
                <dt class="col-sm-3 text-muted">Mekanisme</dt>
                <dd class="col-sm-9 text-info font-bold">{{ $pengajuan->mekanisme->nama }}</dd>
                <dt class="col-sm-3 text-muted">Jenis Izin</dt>
                <dd class="col-sm-9 text-info font-bold">{{ $pengajuan->jenisIzin->nama }}</dd>
                <dt class="col-sm-3 text-muted">Wilayah</dt>
                <dd class="col-sm-9">
                  @foreach($pengajuan->wilayah as $wilayah)
                  <dl class="row">
                    <dt class="col-sm-4 text-muted">Provinsi</dt>
                    <dd class="col-sm-8">{{ $wilayah->provinsi->nama }}</dd>
                    <dt class="col-sm-4 text-muted">Regional</dt>
                    <dd class="col-sm-8">{{ $wilayah->kph->unitKerja->nama }}</dd>
                    <dt class="col-sm-4 text-muted">KPH</dt>
                    <dd class="col-sm-8">{{ $wilayah->kph->nama }}</dd>
                    <dt class="col-sm-4 text-muted">Luas</dt>
                    <dd class="col-sm-8">{{ $wilayah->luas }}</dd>
                  </dl>
                  @endforeach
                </dd>
              </dl>
              <hr>
              <h6 class="text-muted text-center">Kontak Perusahaan</h6>
              <h4 class="text-center font-bold">{{ $pengajuan->pemohon->telp }}</h4>
              <dl class="row m-t-20">
                <dt class="col-sm-3 text-muted">Nama Wali</dt>
                <dd class="col-sm-9">{{ $pengajuan->pemohon->nama_wali }}</dd>
                <dt class="col-sm-3 text-muted">Kontak Wali</dt>
                <dd class="col-sm-9">{{ $pengajuan->pemohon->telp_wali }}</dd>
                <dt class="col-sm-3 text-muted">Jenis Pemohon</dt>
                <dd class="col-sm-9 text-info font-bold">{{ $pengajuan->pemohon->jenisPemohon->nama }}</dd>
              </dl>
            </div>
          </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="two">
          <div class="row">
            <div class="col-sm-12">
              <div class="timeline">
                @php $i=0; @endphp
                @if(count($ProgressPengajuanDetail)!=0)
                  @foreach($ProgressPengajuanDetail as $detail)
                    <article class="timeline-item {{ $i%2 == 0 ? 'alt' : null }}">
                      <div class="timeline-desk">
                        <div class="panel">
                          <div class="panel-body">
                            <span class="arrow-alt"></span>
                            @if($i%2 == 0)
                              <span class="timeline-icon bg-info"><i class="mdi mdi-circle divCircle"></i></span>
                              <span class="timeline-icon bg-warning pointer btn-menu btn-edit" data-id="{{ $detail->id }}" style="margin-right: -25px; margin-top: -24px; display: none;"><i class="mdi mdi-lead-pencil"></i></span>
                              <span class="timeline-icon bgsalmon pointer btn-menu btn-delete" data-id="{{ $detail->id }}" style="margin-right: -25px; margin-top: 2px; display: none;"><i class="mdi mdi-close"></i></span>
                            @else
                              <span class="timeline-icon bg-info"><i class="mdi mdi-circle divCircle"></i></span>
                              <span class="timeline-icon bg-warning pointer btn-menu btn-edit" data-id="{{ $detail->id }}" style="margin-left: -25px; margin-top: -24px; display: none;"><i class="mdi mdi-lead-pencil"></i></span>
                              <span class="timeline-icon bgsalmon pointer btn-menu btn-delete" data-id="{{ $detail->id }}" style="margin-left: -25px; margin-top: 2px; display: none;"><i class="mdi mdi-close"></i></span>
                            @endif
                            <h4 class="text-info"><span class="font-bold">{{ $detail->progress->nama }}</span></h4>
                            <p class="timeline-date text-muted"><small>{{ $detail->created_at->format('d M Y') }}</small></p>
                            <p class="font-13">
                              @foreach($detail->progressPengajuanDetail as $detailKeterangan)
                                {{$detailKeterangan->keterangan->nama}}
                                @if($detailKeterangan->keterangan->type == 'date')
                                  &mdash;<span class="font-bold"> {{ $detailKeterangan->keterangan_date->format('d-m-Y') }}</span><br>
                                @elseif($detailKeterangan->keterangan->type == 'file')
                                  &mdash;<a href="{{ url($detailKeterangan->value) }}" target="_blank"> Lihat</a><br>
                                @elseif($detailKeterangan->keterangan_id == 7)
                                  &mdash;<span class="font-bold">Rp {{ $detailKeterangan->value }}</span><br>
                                @else
                                  &mdash;<span class="font-bold"> {{ $detailKeterangan->value }}</span><br>
                                @endif
                              @endforeach
                            </p>
                          </div>
                        </div>
                      </div>
                    </article>
                  @php $i++ @endphp
                  @endforeach
                @else
                <p>Tidak ada progress</p>
                @endif
              </div>
            </div>
          </div>          
        </div>
        @if(in_array('penggunaan.simpanProgressPengajuan', Auth::user()->routePermissions()))
        <div role="tabpanel" class="tab-pane fade" id="three">
          <div class="row">
            <div class="col-lg-12">
              <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label">Proggress</label>
                <div class="col-sm-6">
                  <select class="form-control" id="progress" name="progressSelect">
                    <option></option>
                    @foreach($ProgressWorkflow as $p)
                      @if(in_array($p->progress_id, $ProgressUsed))
                        <option value="{{$p->progress->id}}">{{ $p->progress->nama }} *</option>
                      @else
                        <option value="{{$p->progress->id}}">{{ $p->progress->nama }}</option>
                      @endif
                    @endforeach
                  </select>
                </div>
              </div>
              <div id="divForm" data-token="{{ csrf_token() }}" data-pengajuan-penggunaan ="{{ $pengajuan->id }}">
              </div>
            </div>
          </div>
        </div>
        @endif
      </div>
    </div>  
  </div>
</div>
@endsection

@section('modal')
  @include('components.penggunaan.peninjauan_lapangan.form_editProgress', compact('pengajuan'))
@endsection

@section('css-top')
  <link href="{{ asset('template/assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
  <link href="{{ asset('template/assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('template/assets/plugins/sweet-alert2/dist/sweetalert2.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('js-bottom')
  <script src="{{ asset('template/assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
  <script src="{{ asset('template/assets/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('template/assets/plugins/sweet-alert2/dist/sweetalert2.all.min.js') }}" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.repeater/1.2.1/jquery.repeater.min.js"></script>
  <script>
    $(function(){  
      $('.datepicker').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'yyyy-mm-dd'
      });
    });
    $('#progress').select2({
      placeholder : 'Pilih Progress'
    });
    $('#progress').on("select2:select", function(){
      var id= $(this).val();
      var baseUrl = "{{ url('/') }}";
      var route = baseUrl+`/pengajuan/penggunaan/getProgressForm/${id}`;
      var divForm = '<form action="'+baseUrl+'/pengajuan/penggunaan/simpanProgressPengajuan" method="post" enctype="multipart/form-data" validate>';

      var token = $('#divForm').data('token');
      var pengajuanPenggunaan = $('#divForm').data('pengajuan-penggunaan');
      divForm += '<input class="form-control" type="hidden" name="_token" value="'+token+'">';
      divForm += '<input class="form-control" type="hidden" name="pengajuanPenggunaanId" value="'+pengajuanPenggunaan+'">';
      divForm += '<input class="form-control" type="hidden" name="progressId" value="'+id+'">';
      $.getJSON(route).then(function(data){
        if (data != '') {
          data.forEach(function(penggunaan) {
            if (penggunaan.keterangan.type == 'file') {
              divForm += '<div class="form-group row">'+
                            '<label for="" class="col-sm-2 col-form-label">'+penggunaan.keterangan.nama+'</label>'+
                            '<div class="col-sm-6">'+
                              '<input class="form-control" type="'+penggunaan.keterangan.type+'" accept="application/pdf,image/*" placeholder="'+penggunaan.keterangan.nama+'" name="lampiran[]" multiple>'+
                              '<input class="form-control" type="hidden" name="lampiranId" value="'+penggunaan.keterangan.id+'">'+
                            '</div>'+
                          '</div>';
            }else{
              divForm += '<div class="form-group row">'+
                            '<label for="" class="col-sm-2 col-form-label">'+penggunaan.keterangan.nama+'</label>'+
                            '<div class="col-sm-6">'+
                              '<input class="form-control" type="'+penggunaan.keterangan.type+'" placeholder="'+penggunaan.keterangan.nama+'" name="keteranganValue[]" required="required">'+
                              '<input class="form-control" type="hidden" name="keteranganId[]" value="'+penggunaan.keterangan.id+'">'+
                            '</div>'+
                          '</div>';
            }
          });
          divForm +='<div class="form-group row">'+
                    '<label for="" class="col-sm-2 col-form-label"></label>'+
                    '<div class="col-sm-6">'+
                    '<button class="btn btn-info">Simpan</button>'+
                    '</div>'+
                  '</div></form>';
        }else{
          var select2Data = $('#progress').select2('data');
          divForm += '<div class="form-group row">'+
                            '<label for="" class="col-sm-2 col-form-label"></label>'+
                            '<div class="col-sm-6">Form Tidak Tersedia'+
                            '</div>'+
                          '</div>';
        }
        $('#divForm').html(divForm);
      })
    })
    $('.divCircle').on('click', function(){
      $(this).parents().children('span.btn-menu').toggle(400)
    })

    $('.btn-delete').on('click', function(){
      $(this).parents('article').addClass('prosesDelete');
      var id = $(this).data('id');
      var baseUrl = "{{ url('/') }}";
      var route = baseUrl+`/pengajuan/penggunaan/deleteProgress/${id}`;
      var token = $('#divForm').data('token');
      Swal.fire({
        title: 'Hapus Progress Pengajuan ?',
        text: "Data yang telah dihapus tidak dapat dikembalikan",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal'
      }).then((result) => {
        if (result.value) {
          $.getJSON(route).then(function(data){
            if (data.result == 'success') {
              Swal.fire({
                  title: "Berhasil!",
                  text: "Progress Pengajuan dihapus.",
                  type: "success",
                  timer: 1500,      
                  showCancelButton: false,
                  showConfirmButton: false
              })
              $('.prosesDelete').hide(2500, function() {
                $(this).remove();
              });
            }else{
              Swal.fire({
                  title: "Tidak Berhasil!",
                  text: "Tidak Berhasil Menghapus Progress Pengajuan.",
                  type: "error",
                  timer: 1500,      
                  showCancelButton: false,
                  showConfirmButton: false
              })
              $('.prosesDelete').removeClass('prosesDelete');
            }
            $('#divForm').html(divForm);
          })
        }else{         
          $('.prosesDelete').removeClass('prosesDelete');
        }
      })
    })

    $('.btn-edit').on('click', function(){
      var id = $(this).data('id');
      var baseUrl = "{{ url('/') }}";
      var route = baseUrl+`/pengajuan/penggunaan/getProgressFormEdit/${id}`;
      var token = $('#divForm').data('token');
      var token = $('#divForm').data('token');
      var divForm = ''
      var fls=true;
      $.getJSON(route).then(function(data){
        var divForm = '<form action="'+baseUrl+'/pengajuan/penggunaan/editProgressPengajuan" method="post" enctype="multipart/form-data" validate>';
        divForm += '<input class="form-control" type="hidden" name="_token" value="'+token+'">';
        divForm += '<input class="form-control" type="hidden" name="id" value="'+id+'">';
        divForm += '<input class="form-control" type="hidden" name="progressId" value="'+data[0].progress.id+'">';
        divForm += '<input class="form-control" type="hidden" name="pengajuanPenggunaanId" value="'+data[0].pengajuan_penggunaan_id+'">';
        data.forEach(function(progressPengajuanDetail) {
          if (progressPengajuanDetail.keterangan.type=='file' && fls) {
            divForm += '<div class="form-group row">'+
                          '<label for="" class="col-sm-3 col-form-label">'+progressPengajuanDetail.keterangan.nama+'</label>'+
                          '<div class="col-sm-9">'+
                            '<input class="form-control" type="'+progressPengajuanDetail.keterangan.type+'" accept="application/pdf,image/*" multiple placeholder="'+progressPengajuanDetail.keterangan.nama+'" name="lampiran[]" required="required">'+
                            '<input class="form-control" type="hidden" name="lampiranId" value="'+progressPengajuanDetail.keterangan.id+'">'+
                          '</div>'+
                        '</div>';
            fls=false;
          }else if(progressPengajuanDetail.keterangan.type=='file' && fls==false){
            divForm +=''
          }else{
            divForm += '<div class="form-group row">'+
                          '<label for="" class="col-sm-3 col-form-label">'+progressPengajuanDetail.keterangan.nama+'</label>'+
                          '<div class="col-sm-9">'+
                            '<input class="form-control" type="'+progressPengajuanDetail.keterangan.type+'" placeholder="'+progressPengajuanDetail.keterangan.nama+'" name="keteranganValue[]" required="required" value="'+progressPengajuanDetail.value+'">'+
                            '<input class="form-control" type="hidden" name="keteranganId[]" value="'+progressPengajuanDetail.keterangan.id+'">'+
                          '</div>'+
                        '</div>';
          }
        }) 
        divForm +='<div class="form-group row">'+
                  '<label for="" class="col-sm-3 col-form-label"></label>'+
                  '<div class="col-sm-9">'+
                  '<button class="btn btn-info">Simpan</button>'+
                  '</div>'+
                '</div></form>';
        $('#divFormEdit').html(divForm)
      })
      $('.modal-form-edit-progress').modal('show');
    })
  </script>
@endsection