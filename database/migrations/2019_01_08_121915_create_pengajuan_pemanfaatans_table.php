<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengajuanPemanfaatansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengajuan_pemanfaatans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kegiatan', 60);
            $table->unsignedInteger('pemohon_id');
            $table->unsignedInteger('kph_id');
            $table->unsignedTinyInteger('unit_kerja_id');
            $table->enum('tingkat', ['direksi', 'divre', 'kph'])->nullable();
            $table->unsignedInteger('disposisi_sekretaris')->nullable();
            $table->boolean('approve_tim')->default(false);
            $table->unsignedInteger('approve_adm')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengajuan_pemanfaatans');
    }
}
