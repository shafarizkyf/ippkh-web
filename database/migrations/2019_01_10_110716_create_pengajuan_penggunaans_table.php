<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengajuanPenggunaansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengajuan_penggunaans', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('pemohon_id');
            $table->text('kegiatan');
            $table->unsignedSmallInteger('jenis_izin_id');
            $table->unsignedSmallInteger('jenis_kegiatan_id');
            $table->unsignedTinyInteger('mekanisme_id');
            $table->unsignedInteger('surat_id')->nullable();
            $table->boolean('is_active')->default(true);
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('changed_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengajuan_penggunaans');
    }
}
