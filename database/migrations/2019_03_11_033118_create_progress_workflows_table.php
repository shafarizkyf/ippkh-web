<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgressWorkflowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('progress_workflows', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('jenis', ['tmkh', 'ppkh', 'kerjasama']);
            $table->unsignedMediumInteger('progress_id');
            $table->unsignedSmallInteger('order');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('progress_workflows');
    }
}
