<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('pengajuan_penggunaan_id')->nullable();
            $table->unsignedInteger('pengajuan_pemanfaatan_id')->nullable();
            $table->string('nama');
            $table->string('fileable_type');
            $table->unsignedInteger('fileable_id');
            $table->text('file');
            $table->enum('type', ['base64', 'path']);
            $table->string('mime', 60)->nullable();
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('changed_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
