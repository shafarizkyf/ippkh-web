<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApprovalPengajuanPemanfaatansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('approval_pengajuan_pemanfaatans', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('pengajuan_pemanfaatan_id');
            $table->unsignedSmallInteger('approval_id');
            $table->unsignedSmallInteger('user_id');
            $table->boolean('is_approve')->default(true);
            $table->text('memo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('approval_pengajuan_pemanfaatans');
    }
}
