<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgressPengajuansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('progress_pengajuans', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('pengajuan_penggunaan_id');
            $table->unsignedMediumInteger('progress_id');
            $table->unsignedMediumInteger('keterangan_id');
            $table->text('value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('progress_pengajuans');
    }
}
