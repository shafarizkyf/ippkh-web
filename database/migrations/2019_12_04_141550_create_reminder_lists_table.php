<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReminderListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR REPLACE VIEW reminder_lists AS SELECT c.pengajuan_penggunaan_id, a.id as reminder_id, a.progress_detail_id, c.progress_id, c.value, DATEDIFF(c.value, now()) as diff_days FROM reminders a, progress_details b, progress_pengajuan_details c WHERE a.progress_detail_id = b.id and c.keterangan_id = b.keterangan_id and c.progress_id = b.progress_id ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reminder_lists');
    }
}
