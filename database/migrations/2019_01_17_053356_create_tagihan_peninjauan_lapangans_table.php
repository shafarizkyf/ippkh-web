<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagihanPeninjauanLapangansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tagihan_peninjauan_lapangans', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('pengajuan_penggunaan_id');
            $table->string('biaya', 20);
            $table->date('tanggal_tenggang', 20);
            $table->date('tanggal_bayar', 20)->nullable();
            $table->unsignedInteger('surat_id')->nullable();
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('changed_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tagihan_peninjauan_lapangans');
    }
}
