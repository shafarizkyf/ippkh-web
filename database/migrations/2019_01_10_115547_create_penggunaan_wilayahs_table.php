<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenggunaanWilayahsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penggunaan_wilayahs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedSmallInteger('provinsi_id');
            $table->unsignedInteger('pengajuan_penggunaan_id');
            $table->unsignedInteger('kph_id');
            $table->float('luas', 6, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penggunaan_wilayahs');
    }
}
