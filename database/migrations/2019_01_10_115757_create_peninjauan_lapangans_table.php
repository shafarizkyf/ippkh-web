<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeninjauanLapangansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peninjauan_lapangans', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('pengajuan_penggunaan_id');
            $table->unsignedInteger('penggunaan_wilayah_id');
            $table->date('tgl_mulai');
            $table->date('tgl_selesai');
            $table->unsignedInteger('surat_id')->nullable();
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('changed_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peninjauan_lapangans');
    }
}
