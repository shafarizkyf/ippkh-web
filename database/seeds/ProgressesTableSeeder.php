<?php

use Illuminate\Database\Seeder;

class ProgressesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = 'seeder/progresses.json';
        $data = json_decode(file_get_contents(storage_path($file)), true);
        DB::table('progresses')->insert($data);                
    }
}
