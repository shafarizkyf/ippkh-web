<?php

use Illuminate\Database\Seeder;

class FilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = 'seeder/files.json';
        $data = json_decode(file_get_contents(storage_path($file)), true);
        DB::table('files')->insert($data);                
    }
}
