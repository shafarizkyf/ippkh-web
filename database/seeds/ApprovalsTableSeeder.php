<?php

use Illuminate\Database\Seeder;

class ApprovalsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = 'seeder/approvals.json';
        $data = json_decode(file_get_contents(storage_path($file)), true);
        DB::table('approvals')->insert($data);
    }
}
