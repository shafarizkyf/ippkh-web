<?php

use Illuminate\Database\Seeder;

class StackholdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = 'seeder/stackholders.json';
        $data = json_decode(file_get_contents(storage_path($file)), true);
        DB::table('stackholders')->insert($data);                
    }
}
