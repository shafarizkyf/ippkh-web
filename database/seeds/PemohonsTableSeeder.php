<?php

use Illuminate\Database\Seeder;

class PemohonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = 'seeder/pemohons.json';
        $data = json_decode(file_get_contents(storage_path($file)), true);
        DB::table('pemohons')->insert($data);                
    }
}
