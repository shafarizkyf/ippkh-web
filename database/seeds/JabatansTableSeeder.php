<?php

use Illuminate\Database\Seeder;

class JabatansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = 'seeder/jabatans.json';
        $data = json_decode(file_get_contents(storage_path($file)), true);
        DB::table('jabatans')->insert($data);                
    }
}
