<?php

use Illuminate\Database\Seeder;

class ApprovalDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $file = 'seeder/approval_details.json';
        $data = json_decode(file_get_contents(storage_path($file)), true);
        DB::table('approval_details')->insert($data);
    }
}
