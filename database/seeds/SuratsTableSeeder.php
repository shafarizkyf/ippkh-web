<?php

use Illuminate\Database\Seeder;

class SuratsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = 'seeder/surats.json';
        $data = json_decode(file_get_contents(storage_path($file)), true);
        DB::table('surats')->insert($data);                
    }
}
