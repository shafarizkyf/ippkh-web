<?php

use Illuminate\Database\Seeder;

class JenisSuratsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = 'seeder/jenis_surats.json';
        $data = json_decode(file_get_contents(storage_path($file)), true);
        DB::table('jenis_surats')->insert($data);
    }
}
