<?php

use Illuminate\Database\Seeder;

class ProgressWorkflowsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = 'seeder/progress_workflows.json';
        $data = json_decode(file_get_contents(storage_path($file)), true);
        DB::table('progress_workflows')->insert($data);                
    }
}
