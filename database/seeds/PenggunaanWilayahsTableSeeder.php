<?php

use Illuminate\Database\Seeder;

class PenggunaanWilayahsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = 'seeder/penggunaan_wilayahs.json';
        $data = json_decode(file_get_contents(storage_path($file)), true);
        DB::table('penggunaan_wilayahs')->insert($data);                
    }
}
