<?php

use Illuminate\Database\Seeder;

class JenisPemohonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = 'seeder/jenis_pemohons.json';
        $data = json_decode(file_get_contents(storage_path($file)), true);
        DB::table('jenis_pemohons')->insert($data);                
    }
}
