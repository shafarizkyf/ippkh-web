<?php

use Illuminate\Database\Seeder;

class PengajuanPenggunaansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = 'seeder/pengajuan_penggunaans.json';
        $data = json_decode(file_get_contents(storage_path($file)), true);
        DB::table('pengajuan_penggunaans')->insert($data);                
    }
}
