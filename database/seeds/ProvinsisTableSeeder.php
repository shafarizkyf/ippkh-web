<?php

use Illuminate\Database\Seeder;

class ProvinsisTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = 'seeder/provinsis.json';
        $data = json_decode(file_get_contents(storage_path($file)), true);
        DB::table('provinsis')->insert($data);                
    }
}
