<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BidangKegiatansTableSeeder::class);
        $this->call(JenisIzinsTableSeeder::class);
        $this->call(JenisKegiatansTableSeeder::class);
        $this->call(JenisPemohonsTableSeeder::class);
        //$this->call(JenisSuratsTableSeeder::class);
        $this->call(KphsTableSeeder::class);
        $this->call(MekanismesTableSeeder::class);
        $this->call(ProvinsisTableSeeder::class);
        $this->call(StackholdersTableSeeder::class);
        $this->call(UnitKerjasTableSeeder::class);
        //$this->call(ApprovalsTableSeeder::class);
        //$this->call(ApprovalDetailsTableSeeder::class);
        $this->call(JabatansTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        //$this->call(SuratsTableSeeder::class);
        //$this->call(FilesTableSeeder::class);
        //$this->call(PemohonsTableSeeder::class);
        //this->call(PengajuanPenggunaansTableSeeder::class);
        //$this->call(PenggunaanWilayahsTableSeeder::class);
        $this->call(KeterangansTableSeeder::class);
        $this->call(ModulsTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(RolePermissionsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(ProgressesTableSeeder::class);
        $this->call(ProgressWorkflowsTableSeeder::class);
    }
}
