<?php

use Illuminate\Database\Seeder;

class MekanismesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = 'seeder/mekanismes.json';
        $data = json_decode(file_get_contents(storage_path($file)), true);
        DB::table('mekanismes')->insert($data);                
    }
}
