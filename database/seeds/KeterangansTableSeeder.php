<?php

use Illuminate\Database\Seeder;

class KeterangansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = 'seeder/keterangans.json';
        $data = json_decode(file_get_contents(storage_path($file)), true);
        DB::table('keterangans')->insert($data);                
    }
}
