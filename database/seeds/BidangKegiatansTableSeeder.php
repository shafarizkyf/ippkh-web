<?php

use Illuminate\Database\Seeder;

class BidangKegiatansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = 'seeder/bidang_kegiatans.json';
        $data = json_decode(file_get_contents(storage_path($file)), true);
        DB::table('bidang_kegiatans')->insert($data);                
    }
}
