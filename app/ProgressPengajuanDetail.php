<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ProgressPengajuanDetail extends Model{

	protected $with = [
		'keterangan'
	];

	protected $appends = ['file_path'];
  
	public function getFilePathAttribute(){
	  return $this->keterangan->type == 'file' ? asset($this->value) : null;
	}
  
	public function getKeteranganDateAttribute(){
		return $this->keterangan->type == 'date' ? Carbon::parse(str_replace('-', '/', $this->value)) : Carbon::now();
	}

	public function keterangan(){
		return $this->belongsTo('App\Keterangan');
	}

	public function pengajuanPenggunaan(){
		return $this->belongsTo('App\PengajuanPenggunaan');
	}
  
	public function progress(){
		return $this->belongsTo('App\Progress');
	}  

	public function progressPengajuan(){
		return $this->belongsTo('App\ProgressPengajuan');
	}
  
}
