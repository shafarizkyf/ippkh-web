<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reminder extends Model {

  public $timestamps = false;

  public function lists(){
    return $this->hasMany('App\ReminderList');
  }

  public function progressDetail(){
    return $this->belongsTo('App\ProgressDetail');
  }

}
