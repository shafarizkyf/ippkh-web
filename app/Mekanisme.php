<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mekanisme extends Model {

  use SoftDeletes;

  public $timestamps = false;

  public static function validate(){
    return request()->validate([
      'nama' => 'required|min:3|max:50',
    ]);
  }

  public function pengajuanPenggunaan() {
    return $this->hasMany('App\PengajuanPenggunaan')->active();
  }

}
