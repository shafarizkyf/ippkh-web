<?php

namespace App\MyClass;

use Browser;

class Reusable {
  public static function isRequestFromApp(){
    return Browser::platformFamily() === "Unknown";
  }

  public static function isRequestFromWeb(){
    return Browser::platformFamily() !== "Unknown";
  }

  public static function moneyFormat($value, $symbol=null){
    return $symbol ? "{$symbol} " . number_format($value, 0, ',', '.') : number_format($value, 0, ',', '.');
  }

  public static function saveBerkas($request, $npwp, $name){
    $path       = "uploads/{$npwp}/berkas";
    $host       = $request->getHttpHost();
    $fileSurat  = $request->file($name);
    $filename   = $fileSurat->getClientOriginalName() . time();
    $filename   = md5($filename) . '.' . $fileSurat->getClientOriginalExtension();
    $filename   = "{$host}/{$path}/{$filename}";
    $mime       = $fileSurat->getMimeType();

    $fileSurat->move($path, $filename);

    return [
      'filename'  => $filename,
      'mime'      => $mime,
      'type'      => 'path',
    ];
  }
  public static function savePengajuanProgressLampiran($request, $npwp, $name='Lampiran'){
    $path       = "uploads/{$npwp}/lampiran";
    $host       = $request->getHost();
    $fileSurat  = $request->file($name);
    $filename   = $fileSurat->getClientOriginalName() . time();
    $filename   = md5($filename) . '.' . $fileSurat->getClientOriginalExtension();
    $filename   = "{$host}/{$path}/{$filename}";
    $mime       = $fileSurat->getMimeType();

    $fileSurat->move($path, $filename);

    return [
      'filename'  => $filename,
      'mime'      => $mime,
      'type'      => 'path',
    ];
  }

  public static function compare($a, $operator, $b){
    switch($operator){
      case '>':
        return $a > $b;
      break;
      case '>=':
        return $a >= $b;
      break;
      case '<':
        return $a < $b;
      break;
      case '<=':
        return $a <= $b;
      break;
      case '==':
        return $a == $b;
      break;
    }
  }

}
