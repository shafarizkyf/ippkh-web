<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pemohon extends Model{

  protected $with = [
    'jenisPemohon'
  ];

  public function changedBy(){
    return $this->belongsTo('App\User', 'changed_by');
  }

  public function createdBy(){
    return $this->belongsTo('App\User', 'created_by');
  }

  public function deletedBy(){
    return $this->belongsTo('App\User', 'deleted_by');
  }
    
  public function jenisPemohon(){
    return $this->belongsTo('App\JenisPemohon');
  }

}
