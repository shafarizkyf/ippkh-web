<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model{

  public $timestamps = false;

  public function hasPermission($id){
    return $this->permission($id)->count();
  }

  public function getActiveAttribute(){
    return $this->is_active ? 'Active' : 'Non-Aktif';
  }

  public function rolePermissions(){
    return $this->hasMany('App\RolePermission');
  }

  public function scopeActive($query, $isActive=true){
    return $query->whereIsActive($isActive);
  }

  public function scopePermission($query, $id){
    return $query->whereHas('rolePermissions', function($q) use ($id){
      $q->wherePermissionId($id);
      $q->whereRoleId($this->id);
    });
  }

}
