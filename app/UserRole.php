<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model{

  public $timestamps = false;

  public function role(){
    return $this->belongsTo('App\Role');
  }

  public function permissions(){
    return $this->hasMany('App\RolePermission', 'role_id', 'role_id')->permissions();
  }

  public function user(){
    return $this->belongsTo('App\User');
  }

}
