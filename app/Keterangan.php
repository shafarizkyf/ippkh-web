<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keterangan extends Model{
  
  public $timestamps = false;
  
  public function progressDetail(){
    return $this->hasMany('App\ProgressDetail');
  }
  
}
