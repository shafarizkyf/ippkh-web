<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Progress extends Model{

  public $timestamps = false;

  public function pengajuan(){
    return $this->hasMany('App\ProgressPengajuan');
  }

  public function progressDetail(){
    return $this->hasMany('App\ProgressDetail');
  }

  public function scopeActive($query, $isActive=true){
    return $query->whereIsActive($isActive);
  }

}
