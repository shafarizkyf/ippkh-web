<?php

namespace App;

use App\Rules\BidangKegiatanExist;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JenisKegiatan extends Model{

  use SoftDeletes;

  public $timestamps = false;

  protected $with = ['bidangKegiatan'];

  public function bidangKegiatan(){
    return $this->belongsTo('App\BidangKegiatan');
  }

  public static function validate(){
    return request()->validate([
      'nama' => 'required|min:3|max:50',
      'bidang_kegiatan_id' => ['required', new BidangKegiatanExist],
    ]);
  }

}

