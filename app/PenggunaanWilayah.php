<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PenggunaanWilayah extends Model{

  public $timestamps = false;
  
  protected $with = [
    'kph'
  ];

  public function kph(){
    return $this->belongsTo('App\Kph');
  }

  public function pengajuanPenggunaan(){
    return $this->belongsTo('App\PengajuanPenggunaan');
  }

  public function provinsi(){
    return $this->belongsTo('App\Provinsi');
  }

}
