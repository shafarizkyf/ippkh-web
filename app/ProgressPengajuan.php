<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ProgressPengajuan extends Model{

	public $appends = ['description'];

	protected $with = ['progress'];

	public function createdBy(){
		return $this->belongsTo('App\User', 'created_by');
	}

	public function getCreatedAtAttribute($value){
		return Carbon::parse($value);
	}

	public function getDescriptionAttribute(){
		$description = '';
		foreach($this->progressPengajuanDetail as $detail){
			if($detail->keterangan->type == 'file'){
				continue;
			}
			$description .= "{$detail->keterangan->nama}: {$detail->value}\n";
		}
		return $description;
	}

	public function pengajuanPenggunaan(){
		return $this->belongsTo('App\PengajuanPenggunaan');
	}

	public function progress(){
		return $this->belongsTo('App\Progress', 'progress_id');
	}

	public function progressPengajuanDetail(){
		return $this->hasMany('App\ProgressPengajuanDetail');
	}

	public function scopeGroupByPengajuan($query){
    return $query->select('pengajuan_penggunaan_id', 'progress_id')
      ->groupBy('progress_id', 'pengajuan_penggunaan_id');
	}

}
