<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PengajuanPemanfaatan extends Model{

  public function files(){
    return $this->morphMany('App\File', 'fileable');
  }

  public function kph(){
    return $this->belongsTo('App\Kph');
  }

  public function pemohon(){
    return $this->belongsTo('App\Pemohon');
  }

  public function unitKerja(){
    return $this->belongsTo('App\UnitKerja');
  }

}
