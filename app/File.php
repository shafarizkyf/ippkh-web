<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class File extends Model{

  protected $appends = [
    'file_link',
    'tagihan'
  ];

  protected $with = [
    'fileable',
    'createdBy'
  ];

  public function changedBy(){
    return $this->belongsTo('App\User', 'changed_by');
  }

  public function createdBy(){
    return $this->belongsTo('App\User', 'created_by');
  }

  public function deletedBy(){
    return $this->belongsTo('App\User', 'deleted_by');
  }

  public function getCreatedAtAttribute($date){
    return Carbon::parse($date);
  }

  public function getFileLinkAttribute(){
    return $this->type == 'path' ? "http://{$this->file}" : "data:{$this->mime};base64,{$this->file}";
  }

  public function getTagihanAttribute(){
    return $this->tagihan();
  }

  public function fileable(){
    return $this->morphTo();
  }

  public function pengajuanPenggunaan(){
    return $this->belongsTo('App\PengajuanPenggunaan', 'pengajuan_penggunaan_id');
  }

  public function surat(){
    if($this->fileable_type == 'App\Surat'){
      return $this->fileable_type::find($this->fileable_id);
    }
  }

  public function tagihan(){
    if($this->fileable_type == 'App\Surat'){
      $surat = $this->surat();
      if($surat->suratable_type == 'App\TagihanPeninjauanLapangan'){
        return $surat->suratable_type::find($surat->suratable_id);
      }
    }

    return null;
  }

}
