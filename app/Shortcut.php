<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shortcut extends Model {

  public $timestamps = false;

  public function progress(){
    return $this->belongsTo('App\Progress');
  }

}
