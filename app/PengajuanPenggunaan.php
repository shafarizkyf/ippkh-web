<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PengajuanPenggunaan extends Model{

  protected $with = [
    'jenisIzin',
    'jenisKegiatan',
    'mekanisme',
    'pemohon',
    'wilayah'
  ];

  protected $appends = ['latest_progress'];

  public function getLatestProgressAttribute(){
    $progress = null;
    if($this->progressPengajuan->count()){
      $progress = $this->progressPengajuan->last();
    }

    return $progress;
  }

  public function changedBy(){
    return $this->belongsTo('App\User', 'changed_by');
  }

  public function createdBy(){
    return $this->belongsTo('App\User', 'created_by');
  }

  public function deletedBy(){
    return $this->belongsTo('App\User', 'deleted_by');
  }

  public function progress(){
    return $this->belongsTo('App\Progress');
  }

  public function dokumen(){
    return $this->hasMany('App\File', 'pengajuan_penggunaan_id');
  }

  public function files(){
    return $this->morphMany('App\File', 'fileable');
  }

  public function jenisIzin(){
    return $this->belongsTo('App\JenisIzin');
  }

  public function jenisKegiatan(){
    return $this->belongsTo('App\JenisKegiatan');
  }
  
  public function mekanisme(){
    return $this->belongsTo('App\Mekanisme');
  }
  
  public function peninjauanLapangan(){
    return $this->hasMany('App\PeninjauanLapangan');
  }

  public function progressPengajuan(){
    return $this->hasMany('App\ProgressPengajuan');
  }

  public function pemohon(){
    return $this->belongsTo('App\Pemohon');
  }

  public function scopeActive($query, $isActive=true){
    return $query->whereIsActive($isActive);
  }

  public function scopeBidangKegiatan($query, $idBidangKegiatan){
    return $query->whereHas('jenisKegiatan', function($q) use ($idBidangKegiatan){
      $q->whereBidangKegiatanId($idBidangKegiatan);
    });
  }

  public function scopeJenisPemohon($query, $idJenisPemohon){
    return $query->whereHas('pemohon', function($q) use ($idJenisPemohon){
      $q->whereJenisPemohonId($idJenisPemohon);
    });
  }

  public function scopeKph($query, $idKph){
    return $query->whereHas('wilayah', function($q) use($idKph){
      $q->whereKphId($idKph);
    });
  }

  public function scopeNamaPemohon($query, $pemohon){
    return $query->whereHas('pemohon', function($q) use ($pemohon){
      $q->where('nama', 'like', "%{$pemohon}%");
    });
  }

  public function scopeIdProgress($query, $progress){
    return $query->whereHas('progressPengajuan', function($q) use ($progress){
      $q->where('progress_id',$progress);
    });
  }
  public function scopeNoSurat($query, $nosurat){
    return $query->whereHas('progressPengajuan.progressPengajuanDetail', function($q) use ($nosurat){
      $q->where('value','like',"%{$nosurat}%");
    });
  }

  public function scopeUnitKerja($query, $idRegional){
    return $query->whereHas('wilayah', function($q) use ($idRegional){
      $idsKph = Kph::whereUnitKerjaId($idRegional)->get()->pluck('id')->toArray();
      $q->whereIn('kph_id', $idsKph);
    });
  }

  public function suratSurat(){
    return $this->morphMany('App\Surat', 'suratable');
  }
  
  public function surat(){
    return $this->belongsTo('App\Surat');
  }

  public function wilayah(){
    return $this->hasMany('App\PenggunaanWilayah');
  }

}
