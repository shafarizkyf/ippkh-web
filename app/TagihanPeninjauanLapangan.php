<?php

namespace App;

use App\MyClass\Reusable;
use Illuminate\Database\Eloquent\Model;

class TagihanPeninjauanLapangan extends Model{

  protected $appends = [
    'biaya_rupiah'
  ];
  
  public function changedBy(){
    return $this->belongsTo('App\User', 'changed_by');
  }

  public function createdBy(){
    return $this->belongsTo('App\User', 'created_by');
  }

  public function deletedBy(){
    return $this->belongsTo('App\User', 'deleted_by');
  }
  
  public function getBiayaRupiahAttribute(){
    return Reusable::moneyFormat($this->biaya, 'Rp');
  }

  public function pengajuanPenggunaan(){
    return $this->belongsTo('App\PengajuanPenggunaan');
  }

  public function surat(){
    return $this->belongsTo('App\Surat');
  }

  public function suratSurat(){
    return $this->morphOne('App\Surat', 'suratable');
  }
  
}
