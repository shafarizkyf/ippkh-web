<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReminderList extends Model {

  public function pengajuanPenggunaan(){
    return $this->belongsTo('App\PengajuanPenggunaan');
  }

  public function progress(){
    return $this->belongsTo('App\Progress');
  }

  public function reminder(){
    return $this->belongsTo('App\Reminder');
  }

}
