<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Surat extends Model{

  protected $with = [
    'createdBy',
    'jenisSurat',
    'pengirim',
    'penerima'
  ];

  public function changedBy(){
    return $this->belongsTo('App\User', 'changed_by');
  }

  public function createdBy(){
    return $this->belongsTo('App\User', 'created_by');
  }

  public function deletedBy(){
    return $this->belongsTo('App\User', 'deleted_by');
  }
    
  public function files(){
    return $this->morphMany('App\File', 'fileable');
  }

  public function getCreatedAtAttribute($time){
    return Carbon::parse($time)->format('d/m/Y');
  }

  public function jenisSurat(){
    return $this->belongsTo('App\JenisSurat');
  }

  public function pengirim(){
    return $this->belongsTo('App\Stackholder', 'pengirim_id');
  }

  public function penerima(){
    return $this->belongsTo('App\Stackholder', 'penerima_id');
  }

  public function suratable(){
    return $this->morphTo();
  }

  public function tagihan(){
    if($this->suratable_type == 'App\TagihanPeninjauanLapangan'){
      return $this->suratable_type::find($this->suratable_id);
    }
  }

}
