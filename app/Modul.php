<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modul extends Model{

  public function permissions(){
    return $this->hasMany('App\Permission');
  }

}
