<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kph extends Model{

  protected $with = [
    'unitKerja'
  ];

  public function unitKerja(){
    return $this->belongsTo('App\UnitKerja');
  }

}
