<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PeninjauanLapangan extends Model{
  
  public function changedBy(){
    return $this->belongsTo('App\User', 'changed_by');
  }

  public function createdBy(){
    return $this->belongsTo('App\User', 'created_by');
  }

  public function deletedBy(){
    return $this->belongsTo('App\User', 'deleted_by');
  }
  
  public function getTglMulaiAttribute($date){
    return Carbon::parse($date);
  }

  public function getTglSelesaiAttribute($date){
    return Carbon::parse($date);
  }
  
  public function pengajuanPenggunaan(){
    return $this->belongsTo('App\PengajuanPenggunaan');
  }

  public function penggunaanWilayah(){
    return $this->belongsTo('App\PenggunaanWilayah');
  }

  public function suratSurat(){
    return $this->morphOne('App\Surat', 'suratable');
  }

  public function surat(){
    return $this->belongsTo('App\Surat');
  }
  
}
