<?php

namespace App\Http\Controllers;

use App\Stackholder;
use Illuminate\Http\Request;

class StackholderController extends Controller{

  public function index(){
    return Stackholder::all();
  }

}
