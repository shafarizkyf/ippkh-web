<?php

namespace App\Http\Controllers;

use App\JenisPemohon;

class JenisPemohonController extends Controller{

  public function index(){
    $jenisPemohons = JenisPemohon::paginate(20);
    return view('jenis_pemohon.index', compact('jenisPemohons'));
  }

  public function store(){
    JenisPemohon::validate();
    $jenisPemohon = new JenisPemohon;
    $jenisPemohon->nama = request('nama');
    $jenisPemohon->save();

    return redirect()->back()->with('success', 'Data berhasil ditambahkan');
  }

  public function update($id){
    JenisPemohon::validate();
    $jenisPemohon = JenisPemohon::findOrFail($id);
    $jenisPemohon->nama = request('nama');
    $jenisPemohon->save();

    return redirect()->back()->with('success', 'Data berhasil diperbarui');
  }

  public function delete($id){
    $jenisPemohon = JenisPemohon::findOrFail($id);
    $jenisPemohon->delete();

    return redirect()->back()->with('success', 'Data berhasil dihapus');
  }

  public function list(){
    return JenisPemohon::all();
  }

}
