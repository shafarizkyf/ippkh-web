<?php

namespace App\Http\Controllers;

use App\Kph;
use Illuminate\Http\Request;

class KphController extends Controller{
  
  public function index(Request $request){
    
    if($request->id_unit_kerja){
      return Kph::whereUnitKerjaId($request->id_unit_kerja)->get();
    }
    
    return Kph::all();
  }
  
}
