<?php

namespace App\Http\Controllers;

use App\Modul;
use App\Permission;
use App\Role;
use App\RolePermission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RoleController extends Controller{

  public function index(){
    $roles = Role::active()->get();
    return view('role.index', compact('roles'));
  }

  public function create(){
    $moduls = Modul::all();
    return view('role.add', compact('moduls'));
  }

  public function store(Request $request){
    DB::transaction(function() use ($request){
      $role = new Role;
      $role->nama = $request->nama;
      $role->save();

      foreach($request->permissions as $id){
        $permission = Permission::find($id);
        $rolePermission = new RolePermission;
        $rolePermission->role()->associate($role);
        $rolePermission->permission()->associate($permission);
        $rolePermission->save();
      }
    });

    return redirect()->back()->with('success', 'Menambah Role');
  }

  public function edit($id){
    $role = Role::find($id);
    $moduls = Modul::all();
    return view('role.edit', compact('role', 'moduls'));
  }

  public function update(Request $request, $id){
    DB::transaction(function() use ($request, $id){
      $role = Role::find($id);
      $role->nama = $request->nama;
      $role->save();

      RolePermission::whereRoleId($id)->delete();
      foreach($request->permissions as $id){
        $permission = Permission::find($id);
        $rolePermission = new RolePermission;
        $rolePermission->role()->associate($role);
        $rolePermission->permission()->associate($permission);
        $rolePermission->save();
      }
    });

    return redirect()->back()->with('success', 'Mengubah Role');
  }


  public function delete($id){
    $role = Role::find($id);
    $role->is_active = false;
    $role->save();

    return redirect()->back()->with('success', 'Menonaktifkan Role');
  }

}
