<?php

namespace App\Http\Controllers;

use App\Progress;
use App\Rules\ProgressExist;
use App\Shortcut;
use Illuminate\Support\Facades\DB;

class ShortcutController extends Controller {

  public function add(){
    $progresses = Progress::active()->get();
    $shortcuts = Shortcut::orderBy('order')->get();
    return view('shortcut.add', compact('progresses', 'shortcuts'));
  }

  public function store(){

    request()->validate([
      'shortcut.*.order' => 'required|numeric',
      'shortcut.*.id_progress' => ['required', new ProgressExist],
    ]);

    DB::transaction(function(){
      Shortcut::truncate();
      $shortcuts = request('shortcut');
      foreach($shortcuts as $data){
        $shortcut = new Shortcut;
        $shortcut->progress_id = $data['id_progress'];
        $shortcut->order = $data['order'];
        $shortcut->save();
      }
    });

    return ['success' => true];
  }

  public function list(){
    $shortcuts = Shortcut::orderBy('order')->get();
    $data = [];
    foreach($shortcuts as $shortcut){
      $data[] = [
        'progress' => $shortcut->progress,
        'total_pengajuan' => $shortcut->progress->pengajuan()->groupByPengajuan()->get()->pluck('pengajuan_penggunaan_id')->count()
      ];
    }

    return $data;
  }

}
