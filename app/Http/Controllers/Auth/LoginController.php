<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class LoginController extends Controller{

  use AuthenticatesUsers;

  protected $username = 'username';

  public function __construct(){
    $this->middleware('guest')->except('logout');
  }

  public function login(Request $request){
    if($request->isMethod('post')){
      if(Auth::attempt($request->only('username', 'password'))){
        return redirect()->route('dashboard');
      }
      return redirect()->back()->with('error', 'Username/Password Salah');
    }
    return view('auth.login');
  }

  public function loginJwt(Request $request){
    $credentials = $request->only('username', 'password');

    try{
      if (!$token = JWTAuth::attempt($credentials)) {
        return response()->json(['error' => 'invalid_credentials'], 401);
      }
    }catch(JWTException $e){
      return response()->json(['error' => 'could_not_create_token'], 500);
    }

    return response()->json(compact('token'));
  }

  public function logout(){
    Auth::logout();
    return redirect()->route('dashboard');
  }

  public function reset(){
    Artisan::call('route:cache');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    return 'reset';
  }

  public function home(){
    if(Auth::check()){
      return redirect()->route('dashboard');
    }

    return redirect(route('login'));
  }

  public function policy(){
    return 'READ_PHONE_STATE does nothing. Just default stuff in react native. will remove soon';
  }

}
