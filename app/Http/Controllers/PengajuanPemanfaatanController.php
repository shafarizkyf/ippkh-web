<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use App\File;
use App\Kph;
use App\Pemohon;
use App\PengajuanPemanfaatan;
use App\JenisPemohon;
use App\Provinsi;
use App\UnitKerja;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class PengajuanPemanfaatanController extends Controller{

  public function index(){
    return view('pemanfaatan.index');
  }

  public function create (){
    $jenisPemohon = JenisPemohon::all();
    $provinsi = Provinsi::all();
    $unitKerja = UnitKerja::all();
    return view('pemanfaatan.add', compact('jenisPemohon', 'provinsi', 'unitKerja'));
  }

  public function store(Request $request){
    Auth::user();
    Route::currentRouteName();

    $response = ['result'=>'fail'];
    DB::transaction(function() use (&$response, $request){

      $jenisPemohon = JenisPemohon::find($request->id_jenis_pemohon);
      $kph = Kph::find($request->id_kph);
      $proposal = $request->file('proposal');
  
      $pemohon = new Pemohon();
      $pemohon->nama = $request->nama;
      $pemohon->npwp = $request->npwp;
      $pemohon->telp = $request->telp;
      $pemohon->nama_wali = $request->nama_wali;
      $pemohon->telp_wali = $request->telp_wali;
      $pemohon->jenisPemohon()->associate($jenisPemohon);
      $pemohon->save();

      $pengajuan = new PengajuanPemanfaatan();
      $pengajuan->kegiatan = $request->kegiatan;
      $pengajuan->pemohon()->associate($pemohon);
      $pengajuan->kph()->associate($kph);
      $pengajuan->unitKerja()->associate($kph->unitKerja);
      $pengajuan->save();

      $path = "uploads";
      $proposal->move($path, $proposal->getClientOriginalName());

      $file = new File();
      $file->nama = 'Proposal';
      $file->file = $proposal->getClientOriginalName();
      $pengajuan->files()->save($file);

      $response = ['result'=>'success'];
    });  

    return $response;
  }
  
}
