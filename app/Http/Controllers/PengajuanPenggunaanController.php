<?php

namespace App\Http\Controllers;

use Auth;
use Browser;
use App\BidangKegiatan;
use App\File;
use App\Kph;
use App\Keterangan;
use App\Mekanisme;
use App\Pemohon;
use App\JenisIzin;
use App\JenisKegiatan;
use App\JenisPemohon;
use App\PengajuanPenggunaan;
use App\PenggunaanWilayah;
use App\Provinsi;
use App\UnitKerja;
use App\MyClass\Reusable;


use App\Progress;
use App\ProgressDetail;
use App\ProgressPengajuan;
use App\ProgressPengajuanDetail;
use App\ProgressWorkflow;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PengajuanPenggunaanController extends Controller{

  public function index(Request $request){
    $jenisPemohon   = JenisPemohon::all();
    $jenisIzin      = JenisIzin::all();
    $bidangKegiatan = BidangKegiatan::all();
    $mekanisme      = Mekanisme::all();
    $provinsi       = Provinsi::all();
    $unitKerja      = UnitKerja::all();
    $progress      = Progress::all();

    $pengajuan = PengajuanPenggunaan::latest()->active();
    $pengajuan = $this->search($pengajuan, $request);
    if(Reusable::isRequestFromWeb()){

      $pengajuan = $request->show_as_laporan 
      ? $pengajuan->latest()->get()
      : $pengajuan->latest()->paginate(30);

      $data = compact(
        'pengajuan',
        'jenisPemohon',
        'jenisIzin',
        'bidangKegiatan',
        'mekanisme',
        'provinsi',
        'unitKerja',
        'progress'
      );

      if($request->show_as_laporan){
        return view('penggunaan.print', $data);
      }

      return view('penggunaan.index', $data);
    }

    //request from mobile api
    return $pengajuan->with(['progressPengajuan'])->get();
  }

  public function create(){
    $jenisPemohon   = JenisPemohon::all();
    $jenisIzin      = JenisIzin::all();
    $bidangKegiatan = BidangKegiatan::all();
    $mekanisme      = Mekanisme::all();
    $provinsi       = Provinsi::all();
    $unitKerja      = UnitKerja::all();

    return view('penggunaan.create', compact(
      'jenisPemohon',
      'jenisIzin',
      'bidangKegiatan',
      'mekanisme',
      'provinsi',
      'unitKerja'
    ));
  }

  public function store(Request $request){

    $request->validate([
      'nama'                => 'required',
      'telp'                => 'required|numeric',
      'nama_wali'           => 'required',
      'telp_wali'           => 'required',
      'id_jenis_pemohon'    => 'required',
      'id_jenis_izin'       => 'required',
      'id_jenis_kegiatan'   => 'required',
      'id_mekanisme'        => 'required',
      'kegiatan'            => 'required',
      'proposal'            => 'required',
      'peta'                => 'required',
      'wilayah'             => 'required',
      'wilayah.*.provinsi'  =>  'required',
      'wilayah.*.kph'       =>  'required',
      'wilayah.*.luas'      =>  'required',
    ]);

    $response = ['result'=>'fail', 'message'=>'Error Occurred'];
    DB::transaction(function() use(&$response, $request) {

      $auth           = Auth::user();
      $jenisPemohon   = JenisPemohon::find($request->id_jenis_pemohon);
      $jenisIzin      = JenisIzin::find($request->id_jenis_izin);
      $jenisKegiatan  = JenisKegiatan::find($request->id_jenis_kegiatan);
      $mekanisme      = Mekanisme::find($request->id_mekanisme);

      $pemohon = new Pemohon();
      $pemohon->nama = $request->nama;
      $pemohon->npwp = $request->npwp;
      $pemohon->telp = $request->telp;
      $pemohon->nama_wali = $request->nama_wali;
      $pemohon->telp_wali = $request->telp_wali;
      $pemohon->jenisPemohon()->associate($jenisPemohon);
      $pemohon->createdBy()->associate($auth);
      $pemohon->save();

      $pengajuan = new PengajuanPenggunaan();
      $pengajuan->kegiatan = $request->kegiatan;
      $pengajuan->pemohon()->associate($pemohon);
      $pengajuan->mekanisme()->associate($mekanisme);
      $pengajuan->jenisIzin()->associate($jenisIzin);
      $pengajuan->jenisKegiatan()->associate($jenisKegiatan);
      $pengajuan->createdBy()->associate($auth);
      $pengajuan->save();

      $proposal = Reusable::saveBerkas($request, $pemohon->npwp, 'proposal');
      $peta = Reusable::saveBerkas($request, $pemohon->npwp, 'peta');

      $file = new File();
      $file->nama = 'Proposal Pengajuan';
      $file->file = $proposal['filename'];
      $file->type = $proposal['type'];
      $file->mime = $proposal['mime'];
      $file->pengajuanPenggunaan()->associate($pengajuan);
      $file->createdBy()->associate($auth);
      $file->save();

      $file = new File();
      $file->nama = 'Peta Pengajuan';
      $file->file = $peta['filename'];
      $file->type = $peta['type'];
      $file->mime = $peta['mime'];
      $file->pengajuanPenggunaan()->associate($pengajuan);
      $file->createdBy()->associate($auth);
      $file->save();

      for($i=0; $i<count($request->wilayah); $i++){
        $kph = Kph::find($request->wilayah[$i]['kph']);
        $provinsi = Provinsi::find($request->wilayah[$i]['provinsi']);

        $wilayah = new PenggunaanWilayah();
        $wilayah->luas = $request->wilayah[$i]['luas'];
        $wilayah->provinsi()->associate($provinsi);
        $wilayah->kph()->associate($kph);
        $wilayah->pengajuanPenggunaan()->associate($pengajuan);
        $wilayah->save();
      }
 
      $response = [
        'result'  => 'success', 
        'id'      => $pengajuan->id,
        'pemohon' => $request->nama,
        'message' => 'Menyimpan data pengajuan'
      ];
    });

    //user using desktop browser not from android app
    if(Reusable::isRequestFromWeb()){
      return redirect()->back()->with($response['result'], $response['message']);
    }

    return $response;
  }

  public function edit(Request $request, $id){
    $jenisPemohon   = JenisPemohon::all();
    $jenisIzin      = JenisIzin::all();
    $bidangKegiatan = BidangKegiatan::all();
    $mekanisme      = Mekanisme::all();
    $provinsi       = Provinsi::all();
    $unitKerja      = UnitKerja::all();
    $pengajuan      = PengajuanPenggunaan::find($id);
    $jenisKegiatan  = JenisKegiatan::whereBidangKegiatanId($pengajuan->jenisKegiatan->bidang_kegiatan_id)->get();

    return view('penggunaan.edit', compact(
      'jenisPemohon',
      'jenisIzin',
      'bidangKegiatan',
      'mekanisme',
      'provinsi',
      'unitKerja',
      'pengajuan',
      'jenisKegiatan'
    ));
  }

  public function update(Request $request, $id){
    $request->validate([
      'nama'                => 'required',
      'telp'                => 'required|numeric',
      'nama_wali'           => 'required',
      'telp_wali'           => 'required',
      'id_jenis_pemohon'    => 'required',
      'id_jenis_izin'       => 'required',
      'id_jenis_kegiatan'   => 'required',
      'id_mekanisme'        => 'required',
      'kegiatan'            => 'required',
      'wilayah'             => 'required',
      'wilayah.*.provinsi'  =>  'required',
      'wilayah.*.kph'       =>  'required',
      'wilayah.*.luas'      =>  'required',
    ]);

    $response = ['result'=>'fail', 'message'=>'Error Occurred'];
    DB::transaction(function() use(&$response, $request, $id) {

      $auth           = Auth::user();
      $jenisPemohon   = JenisPemohon::find($request->id_jenis_pemohon);
      $jenisIzin      = JenisIzin::find($request->id_jenis_izin);
      $jenisKegiatan  = JenisKegiatan::find($request->id_jenis_kegiatan);
      $mekanisme      = Mekanisme::find($request->id_mekanisme);

      $pengajuan = PengajuanPenggunaan::find($id);
      $pengajuan->kegiatan = $request->kegiatan;
      $pengajuan->mekanisme()->associate($mekanisme);
      $pengajuan->jenisIzin()->associate($jenisIzin);
      $pengajuan->jenisKegiatan()->associate($jenisKegiatan);
      $pengajuan->changedBy()->associate($auth);
      $pengajuan->save();

      $pemohon = Pemohon::find($pengajuan->pemohon_id);
      $pemohon->nama = $request->nama;
      $pemohon->npwp = $request->npwp;
      $pemohon->telp = $request->telp;
      $pemohon->nama_wali = $request->nama_wali;
      $pemohon->telp_wali = $request->telp_wali;
      $pemohon->jenisPemohon()->associate($jenisPemohon);
      $pemohon->changedBy()->associate($auth);
      $pemohon->save();

      if($request->file('proposal')){
        $proposal = Reusable::saveBerkas($request, $pemohon->npwp, 'proposal');
  
        $file = File::wherePengajuanPenggunaanId($id)->whereNama('Proposal Pengajuan')->first();
        $file->file = $proposal['filename'];
        $file->type = $proposal['type'];
        $file->mime = $proposal['mime'];
        $file->pengajuanPenggunaan()->associate($pengajuan);
        $file->changedBy()->associate($auth);
        $file->save();
      }

      if($request->file('peta')){
        $peta = Reusable::saveBerkas($request, $pemohon->npwp, 'peta');
        $file = File::wherePengajuanPenggunaanId($id)->whereNama('Peta Pengajuan')->first();
        $file->file = $peta['filename'];
        $file->type = $peta['type'];
        $file->mime = $peta['mime'];
        $file->pengajuanPenggunaan()->associate($pengajuan);
        $file->changedBy()->associate($auth);
        $file->save();
      }


      PenggunaanWilayah::wherePengajuanPenggunaanId($id)->delete();
      for($i=0; $i<count($request->wilayah); $i++){
        $kph = Kph::find($request->wilayah[$i]['kph']);
        $provinsi = Provinsi::find($request->wilayah[$i]['provinsi']);

        $wilayah = new PenggunaanWilayah();
        $wilayah->luas = $request->wilayah[$i]['luas'];
        $wilayah->provinsi()->associate($provinsi);
        $wilayah->kph()->associate($kph);
        $wilayah->pengajuanPenggunaan()->associate($pengajuan);
        $wilayah->save();
      }
 
      $response = [
        'result'  => 'success', 
        'id'      => $pengajuan->id,
        'pemohon' => $request->nama,
        'message' => 'Memperbarui data pengajuan'
      ];
    });

    //user using desktop browser not from android app
    if(Reusable::isRequestFromWeb()){
      return redirect()->route('penggunaan.detail', $id)->with($response['result'], $response['message']);
    }

    return $response;    
  }

  public function detail($id){

    $pengajuan = PengajuanPenggunaan::active()->with([
      'dokumen',
      'progressPengajuan.progress',
      'progressPengajuan.progressPengajuanDetail'
    ])->findOrFail($id);

    $ProgressWorkflow = ProgressWorkflow::whereHas('progress', function($q){
      $q->active();
    })->whereMekanismeId($pengajuan->mekanisme_id)->orderBy('order')->get();

    $ProgressUsed = ProgressPengajuan::wherePengajuanPenggunaanId($id)->wherestatus('1')->pluck('progress_id')->toArray();
    $ProgressPengajuanDetail = ProgressPengajuan::wherePengajuanPenggunaanId($id)->wherestatus('1')->orderBy('created_at', 'DESC')->get();

    if(Reusable::isRequestFromWeb()){
      return view('penggunaan.detail', compact('ProgressWorkflow', 'pengajuan','ProgressUsed', 'ProgressPengajuanDetail'));
    }

    return $pengajuan;
  }

  public function delete($id){
    $pengajuan =  PengajuanPenggunaan::findOrFail($id);
    $pengajuan->is_active = false;
    $pengajuan->save();

    return redirect()->back()->with('success', 'Pengajuan berhasil dihapus');
  }

  public function search($pengajuan, $request){

    if($request->id_jenis_izin){
      $pengajuan = $pengajuan->whereJenisIzinId($request->id_jenis_izin);
    }

    if($request->id_bidang_kegiatan){
      $pengajuan = $pengajuan->bidangKegiatan($request->id_bidang_kegiatan);
    }

    if($request->id_jenis_kegiatan){
      $pengajuan = $pengajuan->whereJenisKegiatanId($request->id_jenis_kegiatan);
    }

    if($request->id_jenis_pemohon){
      $pengajuan = $pengajuan->jenisPemohon($request->id_jenis_pemohon);
    }
    
    if($request->id_mekanisme){
      $pengajuan = $pengajuan->whereMekanismeId($request->id_mekanisme);
    }

    if($request->pemohon){
      $pengajuan = $pengajuan->namaPemohon($request->pemohon);
    }

    if($request->kegiatan){
      $pengajuan = $pengajuan->where('kegiatan', 'like', "%{$request->kegiatan}%");
    }

    if($request->id_unit_kerja){
      $pengajuan = $pengajuan->unitKerja($request->id_unit_kerja);
    }

    if($request->id_kph){
      $pengajuan = $pengajuan->kph($request->id_kph);
    }

    if($request->id_progress){
      $pengajuan = $pengajuan->idProgress($request->id_progress);
    }

    if($request->nosurat){
      $pengajuan = $pengajuan->noSurat($request->nosurat);
    }

    return $pengajuan;
  }

  public function getProgressForm($id){
    $ProgressDetail = ProgressDetail::whereProgressId($id)->with(['progress', 'keterangan'])->orderBy('keterangan_id', 'ASC')->get();
    return $ProgressDetail;
  }

  public function getProgressFormEdit($id){
    $ProgressDetail = ProgressPengajuanDetail::whereProgressPengajuanId($id)->with(['progress', 'keterangan', 'pengajuanPenggunaan', 'progressPengajuan'])->orderBy('keterangan_id', 'ASC')->get();
    return $ProgressDetail;
  }

  public function simpanProgressPengajuan(Request $request){
    $response = ['result'=>'fail', 'message'=>'Error Occurred'];
    DB::transaction(function() use(&$response, $request) {
      $pengajuanPenggunaan        = PengajuanPenggunaan::find($request->pengajuanPenggunaanId);
      $pemohon                    = Pemohon::find($pengajuanPenggunaan->pemohon_id);
      $progress                   = Progress::find($request->progressId);
      $auth                       = Auth::user();
      $progressPengajuan = new ProgressPengajuan();
      $progressPengajuan->pengajuanPenggunaan()->associate($pengajuanPenggunaan);
      $progressPengajuan->progress()->associate($progress);
      $progressPengajuan->createdBy()->associate($auth);
      $progressPengajuan->save();

      for($i=0; $i<count($request->keteranganValue); $i++){
          $progressPengajuanDetail    = new ProgressPengajuanDetail();
          $progress                   = Progress::find($request->progressId);
          $keterangan                 = Keterangan::find($request->keteranganId[$i]);
          $progressPengajuanDetail->progress_pengajuan_id = $progressPengajuan->id;
          $progressPengajuanDetail->pengajuanPenggunaan()->associate($pengajuanPenggunaan);
          $progressPengajuanDetail->progress()->associate($progress);
          $progressPengajuanDetail->keterangan()->associate($keterangan);
          $progressPengajuanDetail->value = $request->keteranganValue[$i];
          $progressPengajuanDetail->save();
      }
      if ($request->file('lampiran')!=null) {
        foreach ($request->file('lampiran') as $lampiran) {
          $path           = 'uploads/'.$pemohon['npwp'].'/lampiran';
          $host           = $request->getHttpHost();
          $filename       = md5($lampiran->getClientOriginalName()) . time() . '.' . $lampiran->getClientOriginalExtension();
          $filename       = "{$path}/{$filename}";
          $mime           = $lampiran->getMimeType();
          $lampiran->move($path, $filename);

          $progressPengajuanDetail      = new ProgressPengajuanDetail();
          $pengajuanPenggunaan          = PengajuanPenggunaan::find($request->pengajuanPenggunaanId);
          $progress                     = Progress::find($request->progressId);
          $keterangan                   = Keterangan::find($request->lampiranId);
          $progressPengajuanDetail->progress_pengajuan_id = $progressPengajuan->id;
          $progressPengajuanDetail->pengajuanPenggunaan()->associate($pengajuanPenggunaan);
          $progressPengajuanDetail->progress()->associate($progress);
          $progressPengajuanDetail->keterangan()->associate($keterangan);
          $progressPengajuanDetail->value = $filename;
          $progressPengajuanDetail->save();
        }
      }
      $response = [
        'result'  => 'success',
        'id'      => '',
        'pemohon' => '',
        'message' => 'Menyimpan Proggress Pengajuan'
      ];
    });

    //user using desktop browser not from android app
    if(Reusable::isRequestFromWeb()){
      return redirect()->back()->with($response['result'], $response['message']);
    }
    return $response;
  }

  public function editProgressPengajuan(Request $request){
    $response = ['result'=>'fail', 'message'=>'Error Occurred'];
    DB::transaction(function() use(&$response, $request) {
      ProgressPengajuanDetail::where('progress_pengajuan_id',$request->id)->delete();

      $progressPengajuan        = ProgressPengajuan::find($request->id)->first();
      $pengajuanPenggunaan      = PengajuanPenggunaan::find($request->pengajuanPenggunaanId);
      $progress                 = Progress::find($request->progressId);
      $pemohon                  = Pemohon::find($pengajuanPenggunaan->pemohon_id);
      for($i=0; $i<count($request->keteranganValue); $i++){
          $progressPengajuanDetail = new ProgressPengajuanDetail();
          $keterangan = Keterangan::find($request->keteranganId[$i]);

          $progressPengajuanDetail->progressPengajuan()->associate($request->id);
          $progressPengajuanDetail->pengajuanPenggunaan()->associate($pengajuanPenggunaan);
          $progressPengajuanDetail->progress()->associate($progress);
          $progressPengajuanDetail->keterangan()->associate($keterangan);
          $progressPengajuanDetail->value = $request->keteranganValue[$i];
          $progressPengajuanDetail->save();
      }
      if ($request->file('lampiran')!=null) {
        foreach ($request->file('lampiran') as $lmprn) {
          $path           = 'uploads/'.$pemohon['npwp'].'/lampiran';
          $host           = $request->getHost();
          $fileLampiran   = $lmprn;
          $filename       = md5('ncun__') . '.' . $fileLampiran->getClientOriginalExtension();
          $filename       = "{$path}/{$filename}";
          $mime           = $fileLampiran->getMimeType();
          $fileLampiran->move($path, $filename);

          $progressPengajuanDetail        = new ProgressPengajuanDetail();
          $pengajuanPenggunaan            = PengajuanPenggunaan::find($request->pengajuanPenggunaanId);
          $progress                       = Progress::find($request->progressId);
          $keterangan                     = Keterangan::find($request->lampiranId);
            $progressPengajuanDetail->progressPengajuan()->associate($request->id);
          $progressPengajuanDetail->pengajuanPenggunaan()->associate($pengajuanPenggunaan);
          $progressPengajuanDetail->progress()->associate($progress);
          $progressPengajuanDetail->keterangan()->associate($keterangan);
          $progressPengajuanDetail->value = $filename;
          $progressPengajuanDetail->save();
        }
      }

      $response = [
        'result'  => 'success', 
        'id'      => '',
        'pemohon' => '',
        'message' => 'Menyimpan Proggress Pengajuan'
      ];
    });

    //user using desktop browser not from android app
    if(Reusable::isRequestFromWeb()){
      return redirect()->back()->with($response['result'], $response['message']);
    }
    return $response;
  }

  public function deleteProgress($id){

    $ProgressPengajuan = ProgressPengajuan::find($id);
    $ProgressPengajuan->status = '0';
    $ProgressPengajuan->save();

    $response = [
      'result'  => 'success', 
      'id'      => '',
      'pemohon' => '',
      'message' => 'Menyimpan Proggress Pengajuan'
    ];
    return($response);
  }

}
