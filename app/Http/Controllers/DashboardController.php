<?php

namespace App\Http\Controllers;

use App\Mekanisme;
use App\ReminderList;
use App\Shortcut;

class DashboardController extends Controller {

  public function index(){
    $reminders = ReminderList::all();
    $mekanismes = Mekanisme::all();
    $shortcuts = Shortcut::orderBy('order')->get();
    return view('dashboard.indexv2', compact('reminders', 'mekanismes', 'shortcuts'));
  }

}
