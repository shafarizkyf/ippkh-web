<?php

namespace App\Http\Controllers;

use App\Mekanisme;
use App\MyClass\Reusable;
use Illuminate\Support\Facades\DB;

class MekanismeController extends Controller{

  public function index(){
    $mekanismes = Mekanisme::all();
    return view('mekanisme.index', compact('mekanismes'));
  }

  public function store(){
    Mekanisme::validate();
    DB::transaction(function(){
      $file = Reusable::saveBerkas(request(), 'contoh', 'file');
      $mekanisme = new Mekanisme;
      $mekanisme->nama = request('nama');
      $mekanisme->panduan = $file['filename'];
      $mekanisme->save();
    });

    return redirect()->back()->with('success', 'Data berhasil ditambahkan');
  }

  public function update($id){
    Mekanisme::validate();
    DB::transaction(function() use ($id){
      $mekanisme = Mekanisme::findOrFail($id);
      $mekanisme->nama = request('nama');

      if(request()->file('file')){
        $file = Reusable::saveBerkas(request(), 'contoh', 'file');
        $mekanisme->panduan = $file['filename'];
      }

      $mekanisme->save();
    });

    return redirect()->back()->with('success', 'Data berhasil diperbarui');
  }

  public function delete($id){
    $mekanisme = Mekanisme::findOrFail($id);
    $mekanisme->delete();

    return redirect()->back()->with('success', 'Data berhasil dihapus');
  }

  public function list(){
    return Mekanisme::all();
  }

}
