<?php

namespace App\Http\Controllers;

use App\Mekanisme;
use App\Progress;
use App\ProgressWorkflow;
use App\Rules\MekanismeExist;
use App\Rules\ProgressExist;
use Illuminate\Http\Request;

class ProgressWorkflowController extends Controller{

  public function index(){
    $workflows = array();
    $mekanisme = ProgressWorkflow::select('mekanisme_id')->groupBy('mekanisme_id')->get();
    foreach($mekanisme as $jenis){
      $workflow = ProgressWorkflow::whereMekanismeId($jenis->mekanisme_id)->with('progress')->get();
      $workflows[] = [
        'mekanisme_id' => $jenis->mekanisme_id,
        'mekanisme' => $jenis->mekanisme->nama,
        'alur' => $workflow,
      ];
    }

    return view('progress_workflow.index', compact('workflows'));
  }

  public function add(){
    $mekanismes = Mekanisme::all();
    $progress = Progress::active()->get();
    return view('progress_workflow.add', compact('progress', 'mekanismes'));
  }

  public function store(){

    request()->validate([
      'mekanisme_id' => ['required', new MekanismeExist],
      'workflow.*.id_progress' => ['required', new ProgressExist],
    ]);

    $workflow = request('workflow');
    $mekanismeId = request('mekanisme_id');

    $mekanisme = Mekanisme::find($mekanismeId);
    $progressWorkflow = ProgressWorkflow::whereMekanismeId($mekanismeId)->count();

    if($progressWorkflow){
      $message = 'Alur untuk mekanisme ' . $mekanisme->nama . ' sudah tersedia';
      return ['success' => false, 'message'=> $message];
    }

    foreach($workflow as $alur){
      $workflow = new ProgressWorkflow;
      $workflow->mekanisme_id = $mekanismeId;
      $workflow->progress_id = $alur['id_progress'];
      $workflow->order = $alur['order'];
      $workflow->save();
    }

    return ['success' => true, 'message'=>'Berhasil menambah alur'];
  }

  public function edit($id){
    $workflow = ProgressWorkflow::whereMekanismeId($id)->orderBy('order')->get();
    $progress = Progress::whereNotIn('id', $workflow->pluck('progress_id')->toArray())->active()->get();
    return view('progress_workflow.edit', compact('workflow', 'progress'));
  }

  public function update(Request $request, $id){
    ProgressWorkflow::whereMekanismeId($id)->delete();
    foreach($request->workflow as $alur){
      $workflow = new ProgressWorkflow;
      $workflow->mekanisme_id = $id;
      $workflow->progress_id = $alur['id_progress'];
      $workflow->order = $alur['order'];
      $workflow->save();
    }

    return ['success' => true, 'message'=>'Berhasil memperbarui data'];
  }

  public function delete($id){
    ProgressWorkflow::whereMekanismeId($id)->delete();
    return redirect()->back()->with('success', 'Menghapus alur progress');
  }

}
