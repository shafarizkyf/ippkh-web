<?php

namespace App\Http\Controllers;

use App\ProgressDetail;
use App\Reminder;
use App\Rules\ProgressDetailExist;
use Illuminate\Support\Facades\DB;

class ReminderController extends Controller {

  public function index(){
    $progresses = ProgressDetail::whereHas('keterangan', function($q){
      $q->whereType('date');
    })->get();

    $reminders = Reminder::orderBy('nama')->get();
    return view('reminder.index', compact('progresses', 'reminders'));
  }

  public function store(){
    $this->validator();
    DB::transaction(function(){
      $days = request('amount');
      $timeType = request('waktu');

      if($timeType == 'year'){
        $days *= 365;
      }elseif($timeType == 'month'){
        $days *= 30;
      }

      $reminder = new Reminder;
      $reminder->progress_detail_id = request('id_progress_detail');
      $reminder->nama = request('nama');
      $reminder->operator = request('operator');
      $reminder->hari = $days;
      $reminder->save();
    });

    return redirect()->back()->with('success', 'Berhasil menambah reminder');
  }

  public function update($id){
    $this->validator();
    DB::transaction(function() use ($id){
      $days = request('amount');
      $timeType = request('waktu');

      if($timeType == 'year'){
        $days *= 365;
      }elseif($timeType == 'month'){
        $days *= 30;
      }

      $reminder = Reminder::findOrFail($id);
      $reminder->progress_detail_id = request('id_progress_detail');
      $reminder->nama = request('nama');
      $reminder->operator = request('operator');
      $reminder->hari = $days;
      $reminder->save();
    });

    return redirect()->back()->with('success', 'Berhasil memperbarui reminder');
  }

  public function delete($id){
    $reminder = Reminder::findOrFail($id);
    $reminder->delete();
    return redirect()->back()->with('success', 'Berhasil menghapus reminder');
  }

  protected function validator(){
    return request()->validate([
      'id_progress_detail' => ['required', new ProgressDetailExist],
      'nama' => 'required|min:3|max:50',
      'operator' => 'required',
      'amount' => 'required|numeric',
      'waktu' => 'required',
    ]);
  }

}
