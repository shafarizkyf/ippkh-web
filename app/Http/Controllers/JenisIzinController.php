<?php

namespace App\Http\Controllers;

use App\JenisIzin;

class JenisIzinController extends Controller{

  public function index(){
    $jenisIzins = JenisIzin::paginate(20);
    return view('jenis_izin.index', compact('jenisIzins'));
  }

  public function store(){
    JenisIzin::validate();
    $jenisIzin = new JenisIzin;
    $jenisIzin->nama = request('nama');
    $jenisIzin->save();

    return redirect()->back()->with('success', 'Data berhasil ditambahkan');
  }

  public function update($id){
    JenisIzin::validate();
    $jenisIzin = JenisIzin::findOrFail($id);
    $jenisIzin->nama = request('nama');
    $jenisIzin->save();

    return redirect()->back()->with('success', 'Data berhasil diperbarui');
  }

  public function delete($id){
    $jenisIzin = JenisIzin::findOrFail($id);
    $jenisIzin->delete();

    return redirect()->back()->with('success', 'Data berhasil dihapus');
  }

  public function list(){
    return JenisIzin::all();
  }

}
