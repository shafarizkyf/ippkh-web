<?php

namespace App\Http\Controllers;

use App\Keterangan;
use App\ProgressPengajuanDetail;

class DokumenController extends Controller {

  public function show($id){
    $fileType = Keterangan::whereType('file')->get()->pluck('id')->toArray();
    return ProgressPengajuanDetail::with([
      'progress',
    ])->wherePengajuanPenggunaanId($id)->whereIn('keterangan_id', $fileType)->get();
  }

}
