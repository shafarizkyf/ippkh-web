<?php

namespace App\Http\Controllers;

use App\File;
use Illuminate\Http\Request;

class FileController extends Controller{

  public function index(Request $request, $id){
    return File::wherePengajuanPenggunaanId($id)->get();
  }

  public function stream(Request $request, $id){
    $file = File::find($id);
    $npwp = $file->pengajuanPenggunaan->pemohon->npwp;

    if($file->type == 'path' && $file->mime == 'application/pdf'){
      $http = "http://{$file->file}";
      $path = explode('/', $file->file);
      $path = "uploads/{$npwp}/surat/{$path[4]}";
      
      return file_exists($path) ? response()->file($path) : 'file not found';
    }

    if($file->type == 'base64' && $file->mime == 'application/pdf'){
      return response()->file($file->file_link);

      $pdfBase64      = $file->file;
      $base64Handler  = fopen($pdfBase64, 'r');
      $pdfContent     = fread ($base64Handler,filesize($pdfBase64));
      fclose($base64Handler);

      $decode   = base64_decode ($pdfContent);
      $filename = "{$file->nama}.pdf";
      $pdf      = fopen ($filename,'w');
      fwrite ($pdf,$decode);
      fclose ($pdf);

      return $pdf;
    }

  }

}
