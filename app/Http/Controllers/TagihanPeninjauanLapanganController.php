<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use App\TagihanPeninjauanLapangan;
use App\PengajuanPenggunaan;
use App\MyClass\Reusable;
use Illuminate\Http\Request;

class TagihanPeninjauanLapanganController extends Controller{

  public function store(Request $request, $id){
    
    $response = ['result'=>'failed', 'message'=>'Error Occured'];
    DB::transaction(function() use ($request, $id, &$response){
      $pengajuan = PengajuanPenggunaan::find($id);
      $npwp = $pengajuan->pemohon->npwp;

      $tagihan = new TagihanPeninjauanLapangan;
      $tagihan->biaya = $request->biaya;
      $tagihan->tanggal_tenggang = date('Y-m-d', strtotime($request->tanggal_tenggang));
      $tagihan->pengajuanPenggunaan()->associate($pengajuan);
      $tagihan->save();

      $suratData = [
        'nama'            => 'Surat Tagihan Biaya Peninjauan Lapangan',
        'nomor'           => $request->nomor,
        'jenis_surat_id'  => 3, //surat perintah cek lapangan
        'pengirim_id'     => 4, //depren
        'penerima_id'     => 1, //pemohon
      ];

      if(Reusable::isRequestFromWeb()){
        $fileData = Reusable::saveSurat($request, $npwp);
      }else{
        $fileData = [
          'filename'  => $request->surat['uri'],
          'mime'      => $request->surat['mime'],
          'type'      => 'base64'
        ];        
      }
  
      $response = (new SuratController)->store($suratData, $fileData, $tagihan);

      $tagihan = TagihanPeninjauanLapangan::find($tagihan->id);
      $tagihan->surat_id = $response['id'];
      $tagihan->save();
    });

    if(Reusable::isRequestFromWeb()){
      return redirect()->back()->with($response['result'], $response['message']);
    }

    return $response;

  }

}
