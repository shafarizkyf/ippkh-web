<?php

namespace App\Http\Controllers;

use DB;
use App\Jabatan;
use Illuminate\Http\Request;

class JabatanController extends Controller{

  public function index(){
    $jabatan = Jabatan::active()->get();
    return view('jabatan.index', compact('jabatan'));
  }

  public function store(Request $request){

    DB::transaction(function() use ($request){
      $jabatan = new Jabatan;
      $jabatan->nama = $request->nama;
      $jabatan->save();
    });

    return redirect()->back()->with('success', 'Menambah data jabatan');
  }

  public function update(Request $request, $id){
    DB::transaction(function() use ($request, $id){
      $jabatan = Jabatan::find($id);
      $jabatan->nama = $request->nama;
      $jabatan->save();
    });

    return redirect()->back()->with('success', 'Mengubah data jabatan');
  }

  public function delete($id){
    DB::transaction(function() use ($id){
      $jabatan = Jabatan::find($id);
      $jabatan->is_active = false;
      $jabatan->save();
    });

    return redirect()->back()->with('success', 'Menghapus data jabatan');
  }

}
