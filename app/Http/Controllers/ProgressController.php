<?php

namespace App\Http\Controllers;

use App\Keterangan;
use App\Progress;
use App\ProgressDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProgressController extends Controller{

  public function index(){
    $progress = Progress::active()->get();
    return view('progress.index', compact('progress'));
  }

  public function add(){
    $keterangan = Keterangan::all();
    return view('progress.add', compact('keterangan'));
  }

  public function edit($id){
    $progress = Progress::active()->findOrFail($id);
    $keterangan = Keterangan::all();
    return view('progress.edit', compact('progress', 'keterangan'));
  }

  public function store(Request $request){
    DB::transaction(function() use ($request) {
      $progress = new Progress;
      $progress->nama = $request->nama;
      $progress->save();

      foreach($request->keterangan as $keterangan){
        $ket = Keterangan::find($keterangan['keterangan_id']);
        $detail = new ProgressDetail;
        $detail->keterangan()->associate($ket);
        $detail->progress()->associate($progress);
        $detail->save();
      }
    });

    return redirect()->back()->with('success', 'Menambah Data Progress');
  }

  public function update(Request $request, $id){
    DB::transaction(function() use ($request, $id) {
      ProgressDetail::whereProgressId($id)->delete();
      foreach($request->keterangan as $keterangan){
        $ket = Keterangan::find($keterangan['keterangan_id']);
        $progress = Progress::find($id);

        $detail = new ProgressDetail;
        $detail->keterangan()->associate($ket);
        $detail->progress()->associate($progress);
        $detail->save();
      }
    });

    return redirect()->back()->with('success', 'Memperbarui Data Progress');
  }

  public function delete($id){
    $progress = Progress::active()->findOrFail($id);
    $progress->is_active = false;
    $progress->save();

    return redirect()->back()->with('success', 'Menghapus komponen progress');
  }
}
