<?php

namespace App\Http\Controllers;

use App\BidangKegiatan;
use App\JenisKegiatan;
use Illuminate\Http\Request;

class JenisKegiatanController extends Controller {

  public function index(){
    $bidangKegiatans = BidangKegiatan::all();
    $jenisKegiatans = JenisKegiatan::orderBy('bidang_kegiatan_id')->get();
    return view('jenis_kegiatan.index', compact('jenisKegiatans', 'bidangKegiatans'));
  }

  public function store(){
    JenisKegiatan::validate();
    $jenisKegiatan = new JenisKegiatan;
    $jenisKegiatan->nama = request('nama');
    $jenisKegiatan->bidang_kegiatan_id = request('bidang_kegiatan_id');
    $jenisKegiatan->save();

    return redirect()->back()->with('success', 'Data berhasil ditambahkan');
  }

  public function update($id){
    JenisKegiatan::validate();
    $jenisKegiatan = JenisKegiatan::findOrFail($id);
    $jenisKegiatan->nama = request('nama');
    $jenisKegiatan->bidang_kegiatan_id = request('bidang_kegiatan_id');
    $jenisKegiatan->save();

    return redirect()->back()->with('success', 'Data berhasil diperbarui');
  }

  public function delete($id){
    $jenisKegiatan = JenisKegiatan::findOrFail($id);
    $jenisKegiatan->delete();

    return redirect()->back()->with('success', 'Data berhasil dihapus');
  }

  public function list(Request $request){

    if($request->id_bidang_kegiatan){
      return JenisKegiatan::whereJenisKegiatanId($request->id_bidang_kegiatan)->with('JenisKegiatan')->get();
    }

    return JenisKegiatan::all();
  }

}
