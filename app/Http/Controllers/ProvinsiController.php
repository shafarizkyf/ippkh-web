<?php

namespace App\Http\Controllers;

use App\Provinsi;
use Illuminate\Http\Request;

class ProvinsiController extends Controller{

  public function index(){
    return Provinsi::all();
  }

}
