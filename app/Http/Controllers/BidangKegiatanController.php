<?php

namespace App\Http\Controllers;

use App\BidangKegiatan;

class BidangKegiatanController extends Controller{

  public function index(){
    $bidangKegiatans = BidangKegiatan::paginate(20);
    return view('bidang_kegiatan.index', compact('bidangKegiatans'));
  }

  public function store(){
    BidangKegiatan::validate();
    $bidangKegiatan = new BidangKegiatan;
    $bidangKegiatan->nama = request('nama');
    $bidangKegiatan->save();

    return redirect()->back()->with('success', 'Data berhasil ditambahkan');
  }

  public function update($id){
    BidangKegiatan::validate();
    $bidangKegiatan = BidangKegiatan::findOrFail($id);
    $bidangKegiatan->nama = request('nama');
    $bidangKegiatan->save();

    return redirect()->back()->with('success', 'Data berhasil diperbarui');
  }

  public function delete($id){
    $bidangKegiatan = BidangKegiatan::findOrFail($id);
    $bidangKegiatan->delete();

    return redirect()->back()->with('success', 'Data berhasil dihapus');
  }

  public function list(){
    return BidangKegiatan::all();
  }

}
