<?php

namespace App\Http\Controllers;

use DB;
use Hash;
use App\User;
use App\UnitKerja;
use App\Kph;
use App\Role;
use Illuminate\Http\Request;

class UserController extends Controller{

  public function index(){
    $users      = User::active()->get();
    $unitKerja  = UnitKerja::all();
    $role       = Role::active()->get();

    return view('pengguna.index', compact('users', 'unitKerja', 'role'));
  }

  public function store(Request $request){
    
    DB::transaction(function() use ($request) {
      $role = Role::find($request->id_role);
      $unitKerja = UnitKerja::find($request->id_unit_kerja);
      $kph = Kph::find($request->id_kph);

      $user = new user;
      $user->nama = $request->nama;
      $user->username = $request->username;
      $user->password = Hash::make($request->password);
      $user->role()->associate($role);
      $user->unitKerja()->associate($unitKerja);
      $user->kph()->associate($kph);
      $user->save();
    });

    return redirect()->back()->with('success', 'Menambah data user');
  }

  public function update(Request $request, $id){
    
    DB::transaction(function() use ($request, $id) {
      $role = Role::find($request->id_role);
      $unitKerja = UnitKerja::find($request->id_unit_kerja);
      $kph = Kph::find($request->id_kph);

      $user = User::find($id);
      $user->nama = $request->nama;
      $user->username = $request->username;
      
      if($request->password){
        $user->password = Hash::make($request->password);
      } 

      $user->role()->associate($role);
      $user->unitKerja()->associate($unitKerja);
      $user->kph()->associate($kph);
      $user->save();
    });

    return redirect()->back()->with('success', 'Memperbarui data user');
  }

  public function delete($id){
    $user = User::active()->find($id);
    $user->is_active = false;
    $user->save();

    return redirect()->back()->with('success', 'Menghapus data user');
  }

}
