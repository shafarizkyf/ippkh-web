<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use App\PengajuanPenggunaan;
use App\PeninjauanLapangan;
use App\PenggunaanWilayah;
use App\MyClass\Reusable;
use Illuminate\Http\Request;

class PeninjauanLapanganController extends Controller{

  public function store(Request $request, $id){
    $response = ['result'=>'fail', 'message'=>'Error Occurred'];
    DB::transaction(function() use ($request, &$response, $id) {

      $pengajuan = PengajuanPenggunaan::find($id);
      $npwp = $pengajuan->pemohon->npwp;

      $suratData = [
        'nama'            => 'Surat Undangan Peninjauan Lapangan',
        'nomor'           => $request->nomor,
        'jenis_surat_id'  => 4, //surat undangan peninjauan lapangan
        'pengirim_id'     => 4, //depren
        'penerima_id'     => 1, //pemohon
      ];
      
      if(Reusable::isRequestFromWeb()){
        $fileData = Reusable::saveSurat($request, $npwp);

      }else{
        $fileData = [
          'filename'  => $request->surat['uri'],
          'mime'      => $request->surat['mime'],
          'type'      => 'base64'
        ];
      }

      for($i=0; $i<count($request->id_kph); $i++){
        $auth             = Auth::user();
        $wilayah          = PenggunaanWilayah::whereKphId($request->id_kph[$i])->wherePengajuanPenggunaanId($id)->first();
        $tanggalMulai     = date('Y-m-d', strtotime($request->tanggal_mulai[$i]));
        $tanggalSelesai   = date('Y-m-d', strtotime($request->tanggal_selesai[$i]));

        $peninjauan = new PeninjauanLapangan();
        $peninjauan->tgl_mulai = $tanggalMulai;
        $peninjauan->tgl_selesai = $tanggalSelesai;
        $peninjauan->pengajuanPenggunaan()->associate($pengajuan);
        $peninjauan->penggunaanWilayah()->associate($wilayah);
        $peninjauan->createdBy()->associate($auth);
        $peninjauan->save();

        $response = (new SuratController)->store($suratData, $fileData, $peninjauan);

        $peninjauan = PeninjauanLapangan::find($peninjauan->id);
        $peninjauan->surat_id = $response['id'];
        $peninjauan->save();
      }
    });

    if(Reusable::isRequestFromWeb()){
      return redirect()->back()->with($response['result'], $response['message']);
    }

    return $response;
  }

}
