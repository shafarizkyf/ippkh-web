<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use App\File;
use App\Surat;
use App\PengajuanPenggunaan;
use App\MyClass\Reusable;
use Illuminate\Http\Request;

class SuratController extends Controller{

  public function index(Request $request, $id){
    return Surat::whereSuratableType('App\PengajuanPenggunaan')
    ->whereSuratableId($id)
    ->get();
  }

  public function storeSuratPerintahPeninjauanLapangan(Request $request, $id){

    $pengajuan  = PengajuanPenggunaan::find($id);
    $npwp       = $pengajuan->pemohon->npwp;

    $suratData = [
      'nama'            => 'Surat Perintah Pemeriksaan Lapangan',
      'nomor'           => $request->nomor,
      'jenis_surat_id'  => 2, //surat perintah cek lapangan
      'pengirim_id'     => 3, //direksi
      'penerima_id'     => 4, //depren
    ];

    if(Reusable::isRequestFromWeb()){
      $fileData = Reusable::saveSurat($request, $npwp);
    }else{
      $fileData = [
        'filename'  => $request->surat['uri'],
        'mime'      => $request->surat['mime'],
        'type'      => 'base64'
      ];    
    }

    $response = $this->store($suratData, $fileData, $pengajuan);
  
    if(Reusable::isRequestFromWeb()){
      return redirect()->back()->with($response['result'], $response['message']);
    }

    return $response;
  }

  public function store($suratData, $fileData, $polyObject){
    
    $response = ['result'=>'failed', 'message'=>'Error Occured'];
    DB::transaction(function() use(&$response, $suratData, $fileData, $polyObject) {

      $id = get_class($polyObject) == 'App\PengajuanPenggunaan' ? $polyObject->id : $polyObject->pengajuan_penggunaan_id;

      $auth = Auth::user();
      $pengajuanPenggunaan = PengajuanPenggunaan::find($id);
  
      $surat = new Surat;
      $surat->nomor = $suratData['nomor'];
      $surat->jenis_surat_id = $suratData['jenis_surat_id'];
      $surat->pengirim_id = $suratData['pengirim_id'];
      $surat->penerima_id = $suratData['penerima_id'];
      $surat->createdBy()->associate($auth);
      $polyObject->suratSurat()->save($surat);
  
      $file = new File;
      $file->nama = $suratData['nama'];
      $file->file = $fileData['filename'];
      $file->type = $fileData['type'];
      $file->mime = $fileData['mime'];
      $file->createdBy()->associate($auth);
      $file->pengajuanPenggunaan()->associate($pengajuanPenggunaan);
      $surat->files()->save($file);

      $response = ['result'=>'success', 'message'=>'Menyimpan data', 'id'=>$surat->id];
    });

    return $response;
  }

}
