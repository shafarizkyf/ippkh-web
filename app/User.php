<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable{
  use Notifiable;
  
  protected $hidden = [
    'password', 'remember_token',
  ];

  public function role(){
    return $this->belongsTo('App\Role');
  }

  public function routePermissions(){
    return $this->role->rolePermissions()->with('permission:id,route_web')->get()->pluck('permission.route_web')->toArray();
  }

  public function kph(){
    return $this->belongsTo('App\Kph');
  }

  public function unitKerja(){
    return $this->belongsTo('App\UnitKerja');
  }

  public function roles(){
    return $this->belongsTo('App\UserRole');
  }

  public function scopeActive($query, $isActive=true){
    return $query->whereIsActive($isActive);
  }

}
