<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgressWorkflow extends Model{

  public $timestamps = false;

  public function mekanisme(){
    return $this->belongsTo('App\Mekanisme');
  }

  public function progress(){
    return $this->belongsTo('App\Progress', 'progress_id');
  }

  public function progressPengajuan(){
    return $this->belongsTo('App\ProgressPengajuan', 'progress_id');
  }

}
