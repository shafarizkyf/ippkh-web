<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatisticByMekanisme extends Model {

  protected $table = 'statictic_by_mekanisme';

  public function mekanisme() {
    return $this->belongsTo('App\Mekanisme');
  }

  public function pengajuanPenggunaan() {
    return $this->belongsTo('App\PengajuanPenggunaan')->active();
  }

  public function progress() {
    return $this->belongsTo('App\Progress');
  }

}
