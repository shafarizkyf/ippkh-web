<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jabatan extends Model{

  public $timestamps = false;

  public function users(){
    return $this->hasMany('App\User');
  }

  public function scopeActive($query, $isActive=true){
    return $query->whereIsActive($isActive);
  }

}
