<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgressDetail extends Model{
  
  public $timestamps = false;
  
  public function keterangan(){
    return $this->belongsTo('App\Keterangan');
  }
  
  public function progress(){
    return $this->belongsTo('App\Progress');
  }
  
}
